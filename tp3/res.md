# TP3 (paul jacobs gwennhaelle pype luc audabram)

## 1 Instruction arithmétique et logique

command : 
```bash
as --32 -g ../toolchain/build/startup/crt0.s -o obj/crt0.o
```
command : 
```bash
as --32 -g logic_arithmetic.s -o obj/logic_arithmetic.o
```
command : 
```bash
ld -melf_i386 -T ../toolchain/build/script/linker_script_minimal.ld obj/crt0.o obj/logic_arithmetic.o -o bin/logic_arithmetic
```
* Quel traitement implémente l’instruction XOR ? Comment aurait-on pu écrire autrement cette même instruction ?

Le traitement implémenté p  r l'instruction xor est un clear du registre eax.
Nous aurions pu faire un mov en passant la valeur 0 dans EAX.
* Des deux instructions implémentant un même traitement proposées à la question précédente, laquelle est la plus optimisée et pourquoi ? Analyser le firmware pour vous aider

Le traitement le plus optimisée est celui du clear car l'action du MOV aurait été sur 5 octets. Le clear prend donc moins de place
command : 
```bash
objdump -S bin/logic_arithmetic
```
<details>
<summary> result of objdump -S bin/logic_arithmetic</summary>

```assembly

bin/logic_arithmetic:     file format elf32-i386


Disassembly of section .text:

080480b4 <_start>:
	.global _start

	.text 
_start:
	push 	%ebp
 80480b4:	55                   	push   %ebp
	mov 	%esp, %ebp
 80480b5:	89 e5                	mov    %esp,%ebp
	call 	main
 80480b7:	e8 07 00 00 00       	call   80480c3 <main>
	mov		$1, %eax
 80480bc:	b8 01 00 00 00       	mov    $0x1,%eax
	int		$0x80
 80480c1:	cd 80                	int    $0x80

080480c3 <main>:
    .global main  

	.text
main:
	xor 	%eax,%eax
 80480c3:	31 c0                	xor    %eax,%eax
	mov 	$9,%ebx
 80480c5:	bb 09 00 00 00       	mov    $0x9,%ebx
	add		%ebx,%eax
 80480ca:	01 d8                	add    %ebx,%eax
	sub		$2,%eax
 80480cc:	83 e8 02             	sub    $0x2,%eax
	ret
 80480cf:	c3                   	ret    
```
</details>


* Quel est le contenu du registre EAX à la fin de l’exécution de la fonction main ?

A la fin de l'execution le registre EAX contient la valeur 7.
## 2 Debugger GDB

command : 
```bash
as --32 -g logic_arithmetic.s -o obj/logic_arithmetic.o
```
command : 
```bash
ld -melf_i386 -T ../toolchain/build/script/linker_script_minimal.ld obj/crt0.o obj/logic_arithmetic.o -o bin/logic_arithmetic
```
command : 
```bash
gdb ./bin/logic_arithmetic
```
* Suivre et comprendre la séquence de commandes GDB proposée ci-dessous

```bash
(gdb) la a
(gdb) l
(gdb) b _start
(gdb) r
(gdb) b main
(gdb) c
(gdb) s
(gdb) i reg eax
(gdb) s
(gdb) i reg eax ebx
(gdb) s
(gdb) i reg eax ebx
(gdb) s
(gdb) i reg eax ebx
(gdb) s
(gdb) s
... until end of program
(gdb) s
```
Ces commandes permettent de voire toutes les interactions entre les registres, ainsi que les commandes assembleurs qui sont exécutés. On peut donc bien voir les actions de mov xor, add...
## 3 Fonction de conversion entier vers ASCII

* Modifier le fichier logic_arithmetic.s afin d’appeler la fonction itoa (integer to string conversion). Assembler les fichiers logic_arithmetic.s , itoa.s et disco/toolchain/build/startup/crt0.s (fichier de startup minimal). En continuité du chapitre sur la compilation et l’édition des liens, utiliser le script linker minimal et réaliser l’édition des liens du projet. Analyser le projet assembleur

command : 
```bash
as --32 -g ../toolchain/build/startup/crt0.s -o obj/crt0.o
```
command : 
```bash
as --32 -g logic_arithmetic_itoa.s -o obj/logic_arithmetic.o
```
command : 
```bash
as --32 -g itoa.s -o obj/itoa.o
```
command : 
```bash
ld -melf_i386 -T ../toolchain/build/script/linker_script_minimal.ld obj/crt0.o obj/logic_arithmetic.o obj/itoa.o -o bin/logic_arithmetic_itoa
```
* que représente le label tab ?

tab est l adresse du tableau tab

command : 

```bash
objdump -S bin/logic_arithmetic_itoa
```
<details>
<summary> result of objdump -S bin/logic_arithmetic_itoa</summary>

```assembly

bin/logic_arithmetic_itoa:     file format elf32-i386


Disassembly of section .text:

080480b4 <_start>:
	.global _start

	.text 
_start:
	push 	%ebp
 80480b4:	55                   	push   %ebp
	mov 	%esp, %ebp
 80480b5:	89 e5                	mov    %esp,%ebp
	call 	main
 80480b7:	e8 07 00 00 00       	call   80480c3 <main>
	mov		$1, %eax
 80480bc:	b8 01 00 00 00       	mov    $0x1,%eax
	int		$0x80
 80480c1:	cd 80                	int    $0x80

080480c3 <main>:
    .global main  

	.text
main:
	xor 	%eax,%eax
 80480c3:	31 c0                	xor    %eax,%eax
	mov 	$9,%ebx
 80480c5:	bb 09 00 00 00       	mov    $0x9,%ebx
	add		%ebx,%eax
 80480ca:	01 d8                	add    %ebx,%eax
	sub		$2,%eax
 80480cc:	83 e8 02             	sub    $0x2,%eax
	call	itoa
 80480cf:	e8 01 00 00 00       	call   80480d5 <itoa>
	ret
 80480d4:	c3                   	ret    

080480d5 <itoa>:
	.zero		1
	.string		"\n"

	.text
itoa:
	add 	$48,%eax
 80480d5:	83 c0 30             	add    $0x30,%eax
	mov 	%al,tab
 80480d8:	a2 08 81 04 08       	mov    %al,0x8048108
	ret
 80480dd:	c3                   	ret    
```
</details>


* Quelle taille fait le tableau d’adresse tab ? Préciser son contenu au démarrage du programme. Proposer une écriture équivalente en langage C

La taille du tableau est de 55, son contenu au démarrage est vide. En language C il faut faire un malloc.

* Que représente la constante décimale 48 et en quoi cela permet une conversion entier vers ASCII (uniquement pour un chiffre compris entre 0 et 9) ?

48 est la valeur de 0 en décimale dans un tableau ASCII, c est un offset
* En utilisant GDB, vérifier la valeur contenu dans EAX après conversion par la fonction itoa et avant la fin de la fonction main (instruction RET).

La valeur de EAX est de 55 (48 + 7, 7 étant la valeur de eax avant l'offset)
## 4 affichage printf

* Modifier le fichier logic_arithmetic.s afin d’appeler la fonction printf. Assembler les fichiers logic_arithmetic.s , itoa.s, printf.s et disco/toolchain/build/startup/crt0.s (fichier de startup minimal). En continuité du chapitre sur la compilation et l’édition des liens, utiliser le script linker minimal et réaliser l’édition des liens du projet. Exécuter le binaire de sortie et analyser le projet assembleur

command : 
```bash
as --32 -g ../toolchain/build/startup/crt0.s -o obj/crt0.o
```
command : 
```bash
as --32 -g logic_arithmetic_printf.s -o obj/logic_arithmetic.o
```
command : 
```bash
as --32 -g itoa.s -o obj/itoa.o
```
command : 
```bash
as --32 -g printf.s -o obj/printf.o
```
command : 
```bash
ld -melf_i386 -T ../toolchain/build/script/linker_script_minimal.ld obj/crt0.o obj/logic_arithmetic.o obj/itoa.o obj/printf.o -o bin/logic_arithmetic_printf
```
command : 
```bash
./bin/logic_arithmetic_printf
```
<details>
<summary> result of ./bin/logic_arithmetic_printf </summary>
```bash
7
```
</details>
## 5 Suite de Fibonacci

* Assembler les fichiers fibonacci.s, itoa.s, printf.s et disco/toolchain/build/startup/crt0.s (fichier de startup minimal). En continuité du chapitre sur la compilation et l’édition des liens, utiliser le script linker minimal et réaliser l’édition des liens du projet. Exécuter le binaire de sortie et analyser le projet assembleur. S’aider de GDB pour votre analyse

command : 
```bash
as --32 -g ../toolchain/build/startup/crt0.s -o obj/crt0.o
```
command : 
```bash
as --32 -g fibonacci.s -o obj/fibonacci.o
```
command : 
```bash
as --32 -g itoa.s -o obj/itoa.o
```
command : 
```bash
as --32 -g printf.s -o obj/printf.o
```
command : 
```bash
ld -melf_i386 -T ../toolchain/build/script/linker_script_minimal.ld obj/crt0.o obj/fibonacci.o obj/itoa.o obj/printf.o -o bin/fibonacci
```
command : 
```bash
./bin/fibonacci
```
command : 
```bash
objdump -S bin/fibonacci
```
<details>
<summary> result of objdump -S bin/fibonacci</summary>

```assembly

bin/fibonacci:     file format elf32-i386


Disassembly of section .text:

080480b4 <_start>:
	.global _start

	.text 
_start:
	push 	%ebp
 80480b4:	55                   	push   %ebp
	mov 	%esp, %ebp
 80480b5:	89 e5                	mov    %esp,%ebp
	call 	main
 80480b7:	e8 07 00 00 00       	call   80480c3 <main>
	mov		$1, %eax
 80480bc:	b8 01 00 00 00       	mov    $0x1,%eax
	int		$0x80
 80480c1:	cd 80                	int    $0x80

080480c3 <main>:
	.comm tmp_eax, 4
	.comm tmp_edx, 4

	.text
main:
	xor 	%eax,%eax
 80480c3:	31 c0                	xor    %eax,%eax
	CONVERT_TO_ASCII_AND_PRINT
 80480c5:	a3 90 81 04 08       	mov    %eax,0x8048190
 80480ca:	89 15 94 81 04 08    	mov    %edx,0x8048194
 80480d0:	e8 6d 00 00 00       	call   8048142 <itoa>
 80480d5:	e8 71 00 00 00       	call   804814b <printf>
 80480da:	a1 90 81 04 08       	mov    0x8048190,%eax
 80480df:	8b 15 94 81 04 08    	mov    0x8048194,%edx
	mov 	$1,%eax
 80480e5:	b8 01 00 00 00       	mov    $0x1,%eax
	CONVERT_TO_ASCII_AND_PRINT
 80480ea:	a3 90 81 04 08       	mov    %eax,0x8048190
 80480ef:	89 15 94 81 04 08    	mov    %edx,0x8048194
 80480f5:	e8 48 00 00 00       	call   8048142 <itoa>
 80480fa:	e8 4c 00 00 00       	call   804814b <printf>
 80480ff:	a1 90 81 04 08       	mov    0x8048190,%eax
 8048104:	8b 15 94 81 04 08    	mov    0x8048194,%edx
	mov		%eax,%esi
 804810a:	89 c6                	mov    %eax,%esi
	mov		$0,%edx
 804810c:	ba 00 00 00 00       	mov    $0x0,%edx
.L0:
	mov		%eax,%edi
 8048111:	89 c7                	mov    %eax,%edi
	add		%esi,%edi
 8048113:	01 f7                	add    %esi,%edi
	mov		%esi,%eax	
 8048115:	89 f0                	mov    %esi,%eax
	mov		%edi,%esi
 8048117:	89 fe                	mov    %edi,%esi
	CONVERT_TO_ASCII_AND_PRINT
 8048119:	a3 90 81 04 08       	mov    %eax,0x8048190
 804811e:	89 15 94 81 04 08    	mov    %edx,0x8048194
 8048124:	e8 19 00 00 00       	call   8048142 <itoa>
 8048129:	e8 1d 00 00 00       	call   804814b <printf>
 804812e:	a1 90 81 04 08       	mov    0x8048190,%eax
 8048133:	8b 15 94 81 04 08    	mov    0x8048194,%edx
	add		$1,%edx
 8048139:	83 c2 01             	add    $0x1,%edx
	cmp		$5,%edx
 804813c:	83 fa 05             	cmp    $0x5,%edx
	jb		.L0	
 804813f:	72 d0                	jb     8048111 <main+0x4e>
	ret
 8048141:	c3                   	ret    

08048142 <itoa>:
	.zero		1
	.string		"\n"

	.text
itoa:
	add 	$48,%eax
 8048142:	83 c0 30             	add    $0x30,%eax
	mov 	%al,tab
 8048145:	a2 8c 81 04 08       	mov    %al,0x804818c
	ret
 804814a:	c3                   	ret    

0804814b <printf>:
    .global printf
   
	.text
printf:
	mov 	$3,%edx
 804814b:	ba 03 00 00 00       	mov    $0x3,%edx
	mov 	$tab,%ecx
 8048150:	b9 8c 81 04 08       	mov    $0x804818c,%ecx
	mov 	$1,%ebx
 8048155:	bb 01 00 00 00       	mov    $0x1,%ebx
	mov 	$4,%eax	
 804815a:	b8 04 00 00 00       	mov    $0x4,%eax
	int 	$0x80
 804815f:	cd 80                	int    $0x80
  	ret
 8048161:	c3                   	ret    
```
</details>


* Pourquoi être passé par une sauvegarde de contexte vers deux variables non initialisées (.comm) tmp_eax et tmp_edx avant d’appeler les fonctions iota et printf ?

Pour pouvoir effectuer les oppérations sans perdre les valeurs
## 6 bibliotheque statique

* Générer une bibliothèque statique (GNU AR ou archiver) et la lier manuellement durant l’édition des liens. Une bibliothèque statique (extension .a ou archive) est une archive soit la concaténation de fichiers objets ELF ré-adressables. Analyser le processus de compilation et d’édition des liens du projet. Valider son bon fonctionnement

command : 
```bash
as --32 -g ../toolchain/build/startup/crt0.s -o obj/crt0.o
```
command : 
```bash
as --32 -g fibonacci.s -o obj/fibonacci.o
```
command : 
```bash
as --32 -g itoa.s -o obj/itoa.o
```
command : 
```bash
as --32 -g printf.s -o obj/printf.o
```
command : 
```bash
ar -rcs lib/libc.a obj/itoa.o obj/printf.o
```
command : 
```bash
ld -melf_i386 -T ../toolchain/build/script/linker_script_minimal.ld obj/crt0.o obj/fibonacci.o lib/libc.a -o bin/fibonacci
```
command : 
```bash
readelf -h lib/libc.a
```
<details>
<summary> result of readelf -h lib/libc.a</summary>

```assembly

File: lib/libc.a(itoa.o)
ELF Header:
  Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF32
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              REL (Relocatable file)
  Machine:                           Intel 80386
  Version:                           0x1
  Entry point address:               0x0
  Start of program headers:          0 (bytes into file)
  Start of section headers:          680 (bytes into file)
  Flags:                             0x0
  Size of this header:               52 (bytes)
  Size of program headers:           0 (bytes)
  Number of program headers:         0
  Size of section headers:           40 (bytes)
  Number of section headers:         17
  Section header string table index: 16

File: lib/libc.a(printf.o)
ELF Header:
  Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF32
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              REL (Relocatable file)
  Machine:                           Intel 80386
  Version:                           0x1
  Entry point address:               0x0
  Start of program headers:          0 (bytes into file)
  Start of section headers:          700 (bytes into file)
  Flags:                             0x0
  Size of this header:               52 (bytes)
  Size of program headers:           0 (bytes)
  Number of program headers:         0
  Size of section headers:           40 (bytes)
  Number of section headers:         17
  Section header string table index: 16
```
</details>

