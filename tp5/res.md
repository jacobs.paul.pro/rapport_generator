# Allocations statiques et fichier ELF

## Variables globales

command : 
```bash
gcc -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall global_variable.c -o global_variable
```
command : 
```bash
ls -lah
```
<details>
<summary> result of ls -lah</summary>

```assembly
total 2.0M
drwxrwxrwx 1 luc luc  512 Jun  5  2021 .
drwxrwxrwx 1 luc luc  512 Jun  3 11:20 ..
-rwxrwxrwx 1 luc luc 1.2K Nov 14  2020 README.md
-rwxrwxrwx 1 luc luc 993K Jun  5  2021 global_variable
-rwxrwxrwx 1 luc luc   99 Nov 14  2020 global_variable.c
-rwxrwxrwx 1 luc luc 978K Jun  3 11:21 global_variable.o
-rwxrwxrwx 1 luc luc  367 Jun  3 11:21 global_variable.s
-rwxrwxrwx 1 luc luc   70 Nov 15  2020 local_static_variable.c
-rwxrwxrwx 1 luc luc  384 Jun  3 11:21 local_static_variable.s
-rwxrwxrwx 1 luc luc  195 Nov 15  2020 string.c
-rwxrwxrwx 1 luc luc 1.3K Jun  3 11:21 string.o
-rwxrwxrwx 1 luc luc  539 Jun  3 11:21 string.s
```
</details>


* Compiler le fichier global_variable.c en s’arrêtant à l’édition des liens. Préciser la taille du fichier objet binaire ELF relogeable de sortie ? Préciser le nombre de sections applicatives, leurs noms ainsi que leurs tailles ? Dans quelle section se trouve le tableau statique tab ? Constater qu’après la compilation, aucune section n’est mappée en mémoire (adresse de base nulle). Elle seront relogées/mappées à l’édition des liens.

Le fichier binaire ELF relogeable de sortie prend 1Mo. 

Il y a 5 sections applicatives :

* .text -> 0000 0012

* .data -> 000f 4240

* .bss -> 0000 0000

* .comment -> 0000 002b

* .note.GNU-stack -> 0000 0000

la tab statique ce trouve dans la section .data d'aprés les tailles des sections.

command : 
```bash
gcc -c -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall global_variable.c -o global_variable.o
```
command : 
```bash
objdump -h global_variable.o
```
<details>
<summary> result of objdump -h global_variable.o</summary>

```assembly

global_variable.o:     file format elf64-x86-64

Sections:
Idx Name          Size      VMA               LMA               File off  Algn
  0 .text         00000012  0000000000000000  0000000000000000  00000040  2**0
                  CONTENTS, ALLOC, LOAD, RELOC, READONLY, CODE
  1 .data         000f4240  0000000000000000  0000000000000000  00000060  2**5
                  CONTENTS, ALLOC, LOAD, DATA
  2 .bss          00000000  0000000000000000  0000000000000000  000f42a0  2**0
                  ALLOC
  3 .comment      0000002b  0000000000000000  0000000000000000  000f42a0  2**0
                  CONTENTS, READONLY
  4 .note.GNU-stack 00000000  0000000000000000  0000000000000000  000f42cb  2**0
                  CONTENTS, READONLY
```
</details>


* Compiler le fichier global_variable.c en s’arrêtant à l’assemblage. Préciser le nom de la référence symbolique (label ou étiquette) représentant l’adresse du tableau tab ? Constater que le tableau n’est pas explicitement placé en mémoire (travail de l’édition des liens) mais qu’il est en revanche explicitement spécifié dans le script assembleur qu’il se situe dans la section .data.

la référence symbolique ce nomme %rip. 

command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall global_variable.c
```
* Le label (ou étiquette ou référence symbolique) main se situe dans quelle section ? Constater qu’un label peut pointer aussi bien sur une section de code (par exemple .text) que sur une section de donnée (par exemple .bss, .rodata ou .data).

Le label ce situe dans la section .text

* Compiler le fichier global_variable.c jusqu’à l’édition des liens incluse. En observant la table des symboles (objdump -t, table contenant des informations sur toutes les références symboliques statiques dont leurs adresses logiques), préciser l’adresse relative du tableau tab après édition des liens ? Analyser le code du programme après désassemblage (objdump -S) et retrouver l’adresse précédemment trouvée dans le code ?

Adresse relative du tableau tab : 0000000000004020.

dans le main on effectue un mov sur 0x2eec(%rip) qui pointe directement sur tab (4020)

command : 
```bash
gcc -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall global_variable.c -o global_variable
```
command : 
```bash
objdump -t global_variable
```
<details>
<summary> result of objdump -t global_variable</summary>

```assembly

global_variable:     file format elf64-x86-64

SYMBOL TABLE:
00000000000002a8 l    d  .interp	0000000000000000              .interp
00000000000002c4 l    d  .note.gnu.build-id	0000000000000000              .note.gnu.build-id
00000000000002e8 l    d  .note.ABI-tag	0000000000000000              .note.ABI-tag
0000000000000308 l    d  .gnu.hash	0000000000000000              .gnu.hash
0000000000000330 l    d  .dynsym	0000000000000000              .dynsym
00000000000003c0 l    d  .dynstr	0000000000000000              .dynstr
000000000000043e l    d  .gnu.version	0000000000000000              .gnu.version
0000000000000450 l    d  .gnu.version_r	0000000000000000              .gnu.version_r
0000000000000470 l    d  .rela.dyn	0000000000000000              .rela.dyn
0000000000001000 l    d  .init	0000000000000000              .init
0000000000001020 l    d  .plt	0000000000000000              .plt
0000000000001030 l    d  .plt.got	0000000000000000              .plt.got
0000000000001040 l    d  .text	0000000000000000              .text
00000000000011b8 l    d  .fini	0000000000000000              .fini
0000000000002000 l    d  .rodata	0000000000000000              .rodata
0000000000002004 l    d  .eh_frame_hdr	0000000000000000              .eh_frame_hdr
0000000000002038 l    d  .eh_frame	0000000000000000              .eh_frame
0000000000003df0 l    d  .init_array	0000000000000000              .init_array
0000000000003df8 l    d  .fini_array	0000000000000000              .fini_array
0000000000003e00 l    d  .dynamic	0000000000000000              .dynamic
0000000000003fc0 l    d  .got	0000000000000000              .got
0000000000004000 l    d  .data	0000000000000000              .data
00000000000f8260 l    d  .bss	0000000000000000              .bss
0000000000000000 l    d  .comment	0000000000000000              .comment
0000000000000000 l    df *ABS*	0000000000000000              crtstuff.c
0000000000001070 l     F .text	0000000000000000              deregister_tm_clones
00000000000010a0 l     F .text	0000000000000000              register_tm_clones
00000000000010e0 l     F .text	0000000000000000              __do_global_dtors_aux
00000000000f8260 l     O .bss	0000000000000001              completed.8060
0000000000003df8 l     O .fini_array	0000000000000000              __do_global_dtors_aux_fini_array_entry
0000000000001120 l     F .text	0000000000000000              frame_dummy
0000000000003df0 l     O .init_array	0000000000000000              __frame_dummy_init_array_entry
0000000000000000 l    df *ABS*	0000000000000000              global_variable.c
0000000000004020 l     O .data	00000000000f4240              tab
0000000000000000 l    df *ABS*	0000000000000000              crtstuff.c
0000000000002104 l     O .eh_frame	0000000000000000              __FRAME_END__
0000000000000000 l    df *ABS*	0000000000000000              
0000000000003df8 l       .init_array	0000000000000000              __init_array_end
0000000000003e00 l     O .dynamic	0000000000000000              _DYNAMIC
0000000000003df0 l       .init_array	0000000000000000              __init_array_start
0000000000002004 l       .eh_frame_hdr	0000000000000000              __GNU_EH_FRAME_HDR
0000000000003fc0 l     O .got	0000000000000000              _GLOBAL_OFFSET_TABLE_
0000000000001000 l     F .init	0000000000000000              _init
00000000000011b0 g     F .text	0000000000000005              __libc_csu_fini
0000000000000000  w      *UND*	0000000000000000              _ITM_deregisterTMCloneTable
0000000000004000  w      .data	0000000000000000              data_start
00000000000f8260 g       .data	0000000000000000              _edata
00000000000011b8 g     F .fini	0000000000000000              .hidden _fini
0000000000000000       F *UND*	0000000000000000              __libc_start_main@@GLIBC_2.2.5
0000000000004000 g       .data	0000000000000000              __data_start
0000000000000000  w      *UND*	0000000000000000              __gmon_start__
0000000000004008 g     O .data	0000000000000000              .hidden __dso_handle
0000000000002000 g     O .rodata	0000000000000004              _IO_stdin_used
0000000000001140 g     F .text	0000000000000065              __libc_csu_init
00000000000f8268 g       .bss	0000000000000000              _end
0000000000001040 g     F .text	000000000000002f              _start
00000000000f8260 g       .bss	0000000000000000              __bss_start
0000000000001129 g     F .text	0000000000000012              main
00000000000f8260 g     O .data	0000000000000000              .hidden __TMC_END__
0000000000000000  w      *UND*	0000000000000000              _ITM_registerTMCloneTable
0000000000000000  w    F *UND*	0000000000000000              __cxa_finalize@@GLIBC_2.2.5


```
</details>


command : 
```bash
objdump -S global_variable
```
<details>
<summary> result of objdump -S global_variable</summary>

```assembly

global_variable:     file format elf64-x86-64


Disassembly of section .init:

0000000000001000 <_init>:
    1000:	f3 0f 1e fa          	endbr64 
    1004:	48 83 ec 08          	sub    $0x8,%rsp
    1008:	48 8b 05 d9 2f 00 00 	mov    0x2fd9(%rip),%rax        # 3fe8 <__gmon_start__>
    100f:	48 85 c0             	test   %rax,%rax
    1012:	74 02                	je     1016 <_init+0x16>
    1014:	ff d0                	callq  *%rax
    1016:	48 83 c4 08          	add    $0x8,%rsp
    101a:	c3                   	retq   

Disassembly of section .plt:

0000000000001020 <.plt>:
    1020:	ff 35 a2 2f 00 00    	pushq  0x2fa2(%rip)        # 3fc8 <_GLOBAL_OFFSET_TABLE_+0x8>
    1026:	ff 25 a4 2f 00 00    	jmpq   *0x2fa4(%rip)        # 3fd0 <_GLOBAL_OFFSET_TABLE_+0x10>
    102c:	0f 1f 40 00          	nopl   0x0(%rax)

Disassembly of section .plt.got:

0000000000001030 <__cxa_finalize@plt>:
    1030:	ff 25 c2 2f 00 00    	jmpq   *0x2fc2(%rip)        # 3ff8 <__cxa_finalize@GLIBC_2.2.5>
    1036:	66 90                	xchg   %ax,%ax

Disassembly of section .text:

0000000000001040 <_start>:
    1040:	f3 0f 1e fa          	endbr64 
    1044:	31 ed                	xor    %ebp,%ebp
    1046:	49 89 d1             	mov    %rdx,%r9
    1049:	5e                   	pop    %rsi
    104a:	48 89 e2             	mov    %rsp,%rdx
    104d:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
    1051:	50                   	push   %rax
    1052:	54                   	push   %rsp
    1053:	4c 8d 05 56 01 00 00 	lea    0x156(%rip),%r8        # 11b0 <__libc_csu_fini>
    105a:	48 8d 0d df 00 00 00 	lea    0xdf(%rip),%rcx        # 1140 <__libc_csu_init>
    1061:	48 8d 3d c1 00 00 00 	lea    0xc1(%rip),%rdi        # 1129 <main>
    1068:	ff 15 72 2f 00 00    	callq  *0x2f72(%rip)        # 3fe0 <__libc_start_main@GLIBC_2.2.5>
    106e:	f4                   	hlt    
    106f:	90                   	nop

0000000000001070 <deregister_tm_clones>:
    1070:	48 8d 3d e9 71 0f 00 	lea    0xf71e9(%rip),%rdi        # f8260 <__TMC_END__>
    1077:	48 8d 05 e2 71 0f 00 	lea    0xf71e2(%rip),%rax        # f8260 <__TMC_END__>
    107e:	48 39 f8             	cmp    %rdi,%rax
    1081:	74 15                	je     1098 <deregister_tm_clones+0x28>
    1083:	48 8b 05 4e 2f 00 00 	mov    0x2f4e(%rip),%rax        # 3fd8 <_ITM_deregisterTMCloneTable>
    108a:	48 85 c0             	test   %rax,%rax
    108d:	74 09                	je     1098 <deregister_tm_clones+0x28>
    108f:	ff e0                	jmpq   *%rax
    1091:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
    1098:	c3                   	retq   
    1099:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000000010a0 <register_tm_clones>:
    10a0:	48 8d 3d b9 71 0f 00 	lea    0xf71b9(%rip),%rdi        # f8260 <__TMC_END__>
    10a7:	48 8d 35 b2 71 0f 00 	lea    0xf71b2(%rip),%rsi        # f8260 <__TMC_END__>
    10ae:	48 29 fe             	sub    %rdi,%rsi
    10b1:	48 89 f0             	mov    %rsi,%rax
    10b4:	48 c1 ee 3f          	shr    $0x3f,%rsi
    10b8:	48 c1 f8 03          	sar    $0x3,%rax
    10bc:	48 01 c6             	add    %rax,%rsi
    10bf:	48 d1 fe             	sar    %rsi
    10c2:	74 14                	je     10d8 <register_tm_clones+0x38>
    10c4:	48 8b 05 25 2f 00 00 	mov    0x2f25(%rip),%rax        # 3ff0 <_ITM_registerTMCloneTable>
    10cb:	48 85 c0             	test   %rax,%rax
    10ce:	74 08                	je     10d8 <register_tm_clones+0x38>
    10d0:	ff e0                	jmpq   *%rax
    10d2:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)
    10d8:	c3                   	retq   
    10d9:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000000010e0 <__do_global_dtors_aux>:
    10e0:	f3 0f 1e fa          	endbr64 
    10e4:	80 3d 75 71 0f 00 00 	cmpb   $0x0,0xf7175(%rip)        # f8260 <__TMC_END__>
    10eb:	75 2b                	jne    1118 <__do_global_dtors_aux+0x38>
    10ed:	55                   	push   %rbp
    10ee:	48 83 3d 02 2f 00 00 	cmpq   $0x0,0x2f02(%rip)        # 3ff8 <__cxa_finalize@GLIBC_2.2.5>
    10f5:	00 
    10f6:	48 89 e5             	mov    %rsp,%rbp
    10f9:	74 0c                	je     1107 <__do_global_dtors_aux+0x27>
    10fb:	48 8b 3d 06 2f 00 00 	mov    0x2f06(%rip),%rdi        # 4008 <__dso_handle>
    1102:	e8 29 ff ff ff       	callq  1030 <__cxa_finalize@plt>
    1107:	e8 64 ff ff ff       	callq  1070 <deregister_tm_clones>
    110c:	c6 05 4d 71 0f 00 01 	movb   $0x1,0xf714d(%rip)        # f8260 <__TMC_END__>
    1113:	5d                   	pop    %rbp
    1114:	c3                   	retq   
    1115:	0f 1f 00             	nopl   (%rax)
    1118:	c3                   	retq   
    1119:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

0000000000001120 <frame_dummy>:
    1120:	f3 0f 1e fa          	endbr64 
    1124:	e9 77 ff ff ff       	jmpq   10a0 <register_tm_clones>

0000000000001129 <main>:
    1129:	55                   	push   %rbp
    112a:	48 89 e5             	mov    %rsp,%rbp
    112d:	c6 05 ec 2e 00 00 47 	movb   $0x47,0x2eec(%rip)        # 4020 <tab>
    1134:	b8 00 00 00 00       	mov    $0x0,%eax
    1139:	5d                   	pop    %rbp
    113a:	c3                   	retq   
    113b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)

0000000000001140 <__libc_csu_init>:
    1140:	f3 0f 1e fa          	endbr64 
    1144:	41 57                	push   %r15
    1146:	4c 8d 3d a3 2c 00 00 	lea    0x2ca3(%rip),%r15        # 3df0 <__frame_dummy_init_array_entry>
    114d:	41 56                	push   %r14
    114f:	49 89 d6             	mov    %rdx,%r14
    1152:	41 55                	push   %r13
    1154:	49 89 f5             	mov    %rsi,%r13
    1157:	41 54                	push   %r12
    1159:	41 89 fc             	mov    %edi,%r12d
    115c:	55                   	push   %rbp
    115d:	48 8d 2d 94 2c 00 00 	lea    0x2c94(%rip),%rbp        # 3df8 <__do_global_dtors_aux_fini_array_entry>
    1164:	53                   	push   %rbx
    1165:	4c 29 fd             	sub    %r15,%rbp
    1168:	48 83 ec 08          	sub    $0x8,%rsp
    116c:	e8 8f fe ff ff       	callq  1000 <_init>
    1171:	48 c1 fd 03          	sar    $0x3,%rbp
    1175:	74 1f                	je     1196 <__libc_csu_init+0x56>
    1177:	31 db                	xor    %ebx,%ebx
    1179:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
    1180:	4c 89 f2             	mov    %r14,%rdx
    1183:	4c 89 ee             	mov    %r13,%rsi
    1186:	44 89 e7             	mov    %r12d,%edi
    1189:	41 ff 14 df          	callq  *(%r15,%rbx,8)
    118d:	48 83 c3 01          	add    $0x1,%rbx
    1191:	48 39 dd             	cmp    %rbx,%rbp
    1194:	75 ea                	jne    1180 <__libc_csu_init+0x40>
    1196:	48 83 c4 08          	add    $0x8,%rsp
    119a:	5b                   	pop    %rbx
    119b:	5d                   	pop    %rbp
    119c:	41 5c                	pop    %r12
    119e:	41 5d                	pop    %r13
    11a0:	41 5e                	pop    %r14
    11a2:	41 5f                	pop    %r15
    11a4:	c3                   	retq   
    11a5:	66 66 2e 0f 1f 84 00 	data16 nopw %cs:0x0(%rax,%rax,1)
    11ac:	00 00 00 00 

00000000000011b0 <__libc_csu_fini>:
    11b0:	f3 0f 1e fa          	endbr64 
    11b4:	c3                   	retq   

Disassembly of section .fini:

00000000000011b8 <_fini>:
    11b8:	f3 0f 1e fa          	endbr64 
    11bc:	48 83 ec 08          	sub    $0x8,%rsp
    11c0:	48 83 c4 08          	add    $0x8,%rsp
    11c4:	c3                   	retq   
```
</details>


* Observer la taille du fichier exécutable de sortie, le nettoyer (stripper) puis ré-observer sa taille sur le média de stockage de masse. Observer également la table des symboles après stripping. Quel traitement a été réalisé ? Le firmware a-t-il été modifié ? Il est à noter qu’il est possible de réaliser un stripping à l’édition des liens en passant l’option -s à GCC.

Taille Avant néttoyage -> 1 016 408 octet

Taille aprés néttoyage -> 1 014 264 octet

La commande strip à supprimer tous les symboles stoqué dans la table.

non le firmware est resté intacte.

command : 
```bash
ls -l
```
<details>
<summary> result of ls -l</summary>

```assembly
total 1985
-rwxrwxrwx 1 luc luc    1176 Nov 14  2020 README.md
-rwxrwxrwx 1 luc luc 1016408 Jun  5  2021 global_variable
-rwxrwxrwx 1 luc luc      99 Nov 14  2020 global_variable.c
-rwxrwxrwx 1 luc luc 1001136 Jun  5 15:59 global_variable.o
-rwxrwxrwx 1 luc luc     367 Jun  5 15:59 global_variable.s
-rwxrwxrwx 1 luc luc      70 Nov 15  2020 local_static_variable.c
-rwxrwxrwx 1 luc luc     384 Jun  3 11:21 local_static_variable.s
-rwxrwxrwx 1 luc luc     195 Nov 15  2020 string.c
-rwxrwxrwx 1 luc luc    1304 Jun  3 11:21 string.o
-rwxrwxrwx 1 luc luc     539 Jun  3 11:21 string.s
```
</details>


command : 
```bash
strip global_variable
```
command : 
```bash
ls -l
```
<details>
<summary> result of ls -l</summary>

```assembly
total 1981
-rwxrwxrwx 1 luc luc    1176 Nov 14  2020 README.md
-rwxrwxrwx 1 luc luc 1014264 Jun  5  2021 global_variable
-rwxrwxrwx 1 luc luc      99 Nov 14  2020 global_variable.c
-rwxrwxrwx 1 luc luc 1001136 Jun  5 15:59 global_variable.o
-rwxrwxrwx 1 luc luc     367 Jun  5 15:59 global_variable.s
-rwxrwxrwx 1 luc luc      70 Nov 15  2020 local_static_variable.c
-rwxrwxrwx 1 luc luc     384 Jun  3 11:21 local_static_variable.s
-rwxrwxrwx 1 luc luc     195 Nov 15  2020 string.c
-rwxrwxrwx 1 luc luc    1304 Jun  3 11:21 string.o
-rwxrwxrwx 1 luc luc     539 Jun  3 11:21 string.s
```
</details>


command : 
```bash
objdump -t global_variable
```
<details>
<summary> result of objdump -t global_variable</summary>

```assembly

global_variable:     file format elf64-x86-64

SYMBOL TABLE:
no symbols


```
</details>


## 2 Variables locales statiques

* Compiler le fichier local_static_variable.c en s’arrêtant à l’assemblage. Préciser le nom de la référence symbolique (label ou étiquette) représentant l’adresse de la variable a ?

la variable "a" a pour référence symbolique %rip

command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall local_static_variable.c
```
* La variable locale statique a se situe-t-elle sur la pile ?

oui, a est en haut de la pile.

* Dans quelle section se situe la variable locale statique a ?

la variable est dans la section : .note.GNU-stack

* Une variable locale statique, tout comme une variable locale standard, ne possède qu’une portée locale à la fonction où elle a été déclarée. En revanche, une variable locale statique mémorise la dernière valeur affectée, même si l’on quitte la fonction et qu’on la rappelle après avoir exécuter le code d’autres fonctions. Ceci est impossible avec une variable locale standard. Pourquoi est-ce possible avec une variable locale statique ?

Une variable statique est déclarée au début du programme et libéré à la fin de celui si, elle est donc accécible à n'importe quel moment dans n'importe quel fonction.

# 3 Chaines de caracteres

command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall string.c
```
* Justifier la taille de la variable gnu_tab ? Où est allouée ce tableau ?

gnu_tab contient 16 caracteres elle fait donc 16 octet. Ce tableau est alloué dans le main via des mov.

* Où (segment ou section) est allouée la chaîne de caractères GNU’s Not Unix ! ? Ma question est-elle rigoureusement formulée ?

non la question n'est pas rigoureuse car nous possédont à aucun moment un point vers la zone mémoire de cette chaine. nous ne pouvons donc pas retrouver son origine.

* Justifier la taille de la variable gnu_pointer ?

la variable gnu_pointer pése 4 octet car c'est un pointeur.

* Où (segment ou section) est allouée la chaîne de caractères GNU's design is Unix-like but differs by being free software and containing no Unix code ! ? S’agit-il d’une allocation statique ou d’une allocation dynamique sur la pile ?

La chaine est alloué directement depuis le langage assembler dans la section .LC0. c'est une allocation dynamique.

* Observer les chaînes de caractères en observant les contenus binaires des sections applicatives du fichier ELF relogeable après compilation mais avant édition des liens.

command : 
```bash
gcc -c -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall string.c -o string.o
```
command : 
```bash
objdump -s string.o
```
<details>
<summary> result of objdump -s string.o</summary>

```assembly

string.o:     file format elf64-x86-64

Contents of section .text:
 0000 554889e5 48b8474e 55277320 4e6f48ba  UH..H.GNU's NoH.
 0010 7420556e 69782021 488945e0 488955e8  t Unix !H.E.H.U.
 0020 c645f000 48c745f8 00000000 b8000000  .E..H.E.........
 0030 005dc3                               .].             
Contents of section .rodata:
 0000 474e5527 73206465 7369676e 20697320  GNU's design is 
 0010 556e6978 2d6c696b 65206275 74206469  Unix-like but di
 0020 66666572 73206279 20626569 6e672066  ffers by being f
 0030 72656520 736f6674 77617265 20616e64  ree software and
 0040 20636f6e 7461696e 696e6720 6e6f2055   containing no U
 0050 6e697820 636f6465 202100             nix code !.     
Contents of section .comment:
 0000 00474343 3a202855 62756e74 7520392e  .GCC: (Ubuntu 9.
 0010 332e302d 31377562 756e7475 317e3230  3.0-17ubuntu1~20
 0020 2e303429 20392e33 2e3000             .04) 9.3.0.     
```
</details>


