# Compilation et edition de lien

command : 
```bash
gcc -Wall -m32 -I./include/ ./src/hello.c -o ./build/bin/hello
```
command : 
```bash
objdump -S ./build/bin/hello
```
<details>
<summary> result of objdump -S ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386


Disassembly of section .init:

00001000 <_init>:
    1000:	f3 0f 1e fb          	endbr32 
    1004:	53                   	push   %ebx
    1005:	83 ec 08             	sub    $0x8,%esp
    1008:	e8 83 00 00 00       	call   1090 <__x86.get_pc_thunk.bx>
    100d:	81 c3 f3 2f 00 00    	add    $0x2ff3,%ebx
    1013:	8b 83 f4 ff ff ff    	mov    -0xc(%ebx),%eax
    1019:	85 c0                	test   %eax,%eax
    101b:	74 02                	je     101f <_init+0x1f>
    101d:	ff d0                	call   *%eax
    101f:	83 c4 08             	add    $0x8,%esp
    1022:	5b                   	pop    %ebx
    1023:	c3                   	ret    

Disassembly of section .plt:

00001030 <__libc_start_main@plt-0x10>:
    1030:	ff b3 04 00 00 00    	push   0x4(%ebx)
    1036:	ff a3 08 00 00 00    	jmp    *0x8(%ebx)
    103c:	00 00                	add    %al,(%eax)
	...

00001040 <__libc_start_main@plt>:
    1040:	ff a3 0c 00 00 00    	jmp    *0xc(%ebx)
    1046:	68 00 00 00 00       	push   $0x0
    104b:	e9 e0 ff ff ff       	jmp    1030 <_init+0x30>

Disassembly of section .text:

00001050 <_start>:
    1050:	f3 0f 1e fb          	endbr32 
    1054:	31 ed                	xor    %ebp,%ebp
    1056:	5e                   	pop    %esi
    1057:	89 e1                	mov    %esp,%ecx
    1059:	83 e4 f0             	and    $0xfffffff0,%esp
    105c:	50                   	push   %eax
    105d:	54                   	push   %esp
    105e:	52                   	push   %edx
    105f:	e8 22 00 00 00       	call   1086 <_start+0x36>
    1064:	81 c3 9c 2f 00 00    	add    $0x2f9c,%ebx
    106a:	8d 83 10 d2 ff ff    	lea    -0x2df0(%ebx),%eax
    1070:	50                   	push   %eax
    1071:	8d 83 a0 d1 ff ff    	lea    -0x2e60(%ebx),%eax
    1077:	50                   	push   %eax
    1078:	51                   	push   %ecx
    1079:	56                   	push   %esi
    107a:	ff b3 f8 ff ff ff    	push   -0x8(%ebx)
    1080:	e8 bb ff ff ff       	call   1040 <__libc_start_main@plt>
    1085:	f4                   	hlt    
    1086:	8b 1c 24             	mov    (%esp),%ebx
    1089:	c3                   	ret    
    108a:	66 90                	xchg   %ax,%ax
    108c:	66 90                	xchg   %ax,%ax
    108e:	66 90                	xchg   %ax,%ax

00001090 <__x86.get_pc_thunk.bx>:
    1090:	8b 1c 24             	mov    (%esp),%ebx
    1093:	c3                   	ret    
    1094:	66 90                	xchg   %ax,%ax
    1096:	66 90                	xchg   %ax,%ax
    1098:	66 90                	xchg   %ax,%ax
    109a:	66 90                	xchg   %ax,%ax
    109c:	66 90                	xchg   %ax,%ax
    109e:	66 90                	xchg   %ax,%ax

000010a0 <deregister_tm_clones>:
    10a0:	e8 e4 00 00 00       	call   1189 <__x86.get_pc_thunk.dx>
    10a5:	81 c2 5b 2f 00 00    	add    $0x2f5b,%edx
    10ab:	8d 8a 18 00 00 00    	lea    0x18(%edx),%ecx
    10b1:	8d 82 18 00 00 00    	lea    0x18(%edx),%eax
    10b7:	39 c8                	cmp    %ecx,%eax
    10b9:	74 1d                	je     10d8 <deregister_tm_clones+0x38>
    10bb:	8b 82 ec ff ff ff    	mov    -0x14(%edx),%eax
    10c1:	85 c0                	test   %eax,%eax
    10c3:	74 13                	je     10d8 <deregister_tm_clones+0x38>
    10c5:	55                   	push   %ebp
    10c6:	89 e5                	mov    %esp,%ebp
    10c8:	83 ec 14             	sub    $0x14,%esp
    10cb:	51                   	push   %ecx
    10cc:	ff d0                	call   *%eax
    10ce:	83 c4 10             	add    $0x10,%esp
    10d1:	c9                   	leave  
    10d2:	c3                   	ret    
    10d3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    10d7:	90                   	nop
    10d8:	c3                   	ret    
    10d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

000010e0 <register_tm_clones>:
    10e0:	e8 a4 00 00 00       	call   1189 <__x86.get_pc_thunk.dx>
    10e5:	81 c2 1b 2f 00 00    	add    $0x2f1b,%edx
    10eb:	55                   	push   %ebp
    10ec:	89 e5                	mov    %esp,%ebp
    10ee:	53                   	push   %ebx
    10ef:	8d 8a 18 00 00 00    	lea    0x18(%edx),%ecx
    10f5:	8d 82 18 00 00 00    	lea    0x18(%edx),%eax
    10fb:	83 ec 04             	sub    $0x4,%esp
    10fe:	29 c8                	sub    %ecx,%eax
    1100:	89 c3                	mov    %eax,%ebx
    1102:	c1 e8 1f             	shr    $0x1f,%eax
    1105:	c1 fb 02             	sar    $0x2,%ebx
    1108:	01 d8                	add    %ebx,%eax
    110a:	d1 f8                	sar    %eax
    110c:	74 14                	je     1122 <register_tm_clones+0x42>
    110e:	8b 92 fc ff ff ff    	mov    -0x4(%edx),%edx
    1114:	85 d2                	test   %edx,%edx
    1116:	74 0a                	je     1122 <register_tm_clones+0x42>
    1118:	83 ec 08             	sub    $0x8,%esp
    111b:	50                   	push   %eax
    111c:	51                   	push   %ecx
    111d:	ff d2                	call   *%edx
    111f:	83 c4 10             	add    $0x10,%esp
    1122:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    1125:	c9                   	leave  
    1126:	c3                   	ret    
    1127:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    112e:	66 90                	xchg   %ax,%ax

00001130 <__do_global_dtors_aux>:
    1130:	f3 0f 1e fb          	endbr32 
    1134:	55                   	push   %ebp
    1135:	89 e5                	mov    %esp,%ebp
    1137:	53                   	push   %ebx
    1138:	e8 53 ff ff ff       	call   1090 <__x86.get_pc_thunk.bx>
    113d:	81 c3 c3 2e 00 00    	add    $0x2ec3,%ebx
    1143:	83 ec 04             	sub    $0x4,%esp
    1146:	80 bb 18 00 00 00 00 	cmpb   $0x0,0x18(%ebx)
    114d:	75 28                	jne    1177 <__do_global_dtors_aux+0x47>
    114f:	8b 83 f0 ff ff ff    	mov    -0x10(%ebx),%eax
    1155:	85 c0                	test   %eax,%eax
    1157:	74 12                	je     116b <__do_global_dtors_aux+0x3b>
    1159:	83 ec 0c             	sub    $0xc,%esp
    115c:	ff b3 14 00 00 00    	push   0x14(%ebx)
    1162:	ff 93 f0 ff ff ff    	call   *-0x10(%ebx)
    1168:	83 c4 10             	add    $0x10,%esp
    116b:	e8 30 ff ff ff       	call   10a0 <deregister_tm_clones>
    1170:	c6 83 18 00 00 00 01 	movb   $0x1,0x18(%ebx)
    1177:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    117a:	c9                   	leave  
    117b:	c3                   	ret    
    117c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00001180 <frame_dummy>:
    1180:	f3 0f 1e fb          	endbr32 
    1184:	e9 57 ff ff ff       	jmp    10e0 <register_tm_clones>

00001189 <__x86.get_pc_thunk.dx>:
    1189:	8b 14 24             	mov    (%esp),%edx
    118c:	c3                   	ret    

0000118d <main>:
    118d:	55                   	push   %ebp
    118e:	89 e5                	mov    %esp,%ebp
    1190:	e8 07 00 00 00       	call   119c <__x86.get_pc_thunk.ax>
    1195:	05 6b 2e 00 00       	add    $0x2e6b,%eax
    119a:	eb fe                	jmp    119a <main+0xd>

0000119c <__x86.get_pc_thunk.ax>:
    119c:	8b 04 24             	mov    (%esp),%eax
    119f:	c3                   	ret    

000011a0 <__libc_csu_init>:
    11a0:	f3 0f 1e fb          	endbr32 
    11a4:	55                   	push   %ebp
    11a5:	e8 6b 00 00 00       	call   1215 <__x86.get_pc_thunk.bp>
    11aa:	81 c5 56 2e 00 00    	add    $0x2e56,%ebp
    11b0:	57                   	push   %edi
    11b1:	56                   	push   %esi
    11b2:	53                   	push   %ebx
    11b3:	83 ec 0c             	sub    $0xc,%esp
    11b6:	89 eb                	mov    %ebp,%ebx
    11b8:	8b 7c 24 28          	mov    0x28(%esp),%edi
    11bc:	e8 3f fe ff ff       	call   1000 <_init>
    11c1:	8d 9d f8 fe ff ff    	lea    -0x108(%ebp),%ebx
    11c7:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
    11cd:	29 c3                	sub    %eax,%ebx
    11cf:	c1 fb 02             	sar    $0x2,%ebx
    11d2:	74 29                	je     11fd <__libc_csu_init+0x5d>
    11d4:	31 f6                	xor    %esi,%esi
    11d6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    11dd:	8d 76 00             	lea    0x0(%esi),%esi
    11e0:	83 ec 04             	sub    $0x4,%esp
    11e3:	57                   	push   %edi
    11e4:	ff 74 24 2c          	push   0x2c(%esp)
    11e8:	ff 74 24 2c          	push   0x2c(%esp)
    11ec:	ff 94 b5 f4 fe ff ff 	call   *-0x10c(%ebp,%esi,4)
    11f3:	83 c6 01             	add    $0x1,%esi
    11f6:	83 c4 10             	add    $0x10,%esp
    11f9:	39 f3                	cmp    %esi,%ebx
    11fb:	75 e3                	jne    11e0 <__libc_csu_init+0x40>
    11fd:	83 c4 0c             	add    $0xc,%esp
    1200:	5b                   	pop    %ebx
    1201:	5e                   	pop    %esi
    1202:	5f                   	pop    %edi
    1203:	5d                   	pop    %ebp
    1204:	c3                   	ret    
    1205:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    120c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00001210 <__libc_csu_fini>:
    1210:	f3 0f 1e fb          	endbr32 
    1214:	c3                   	ret    

00001215 <__x86.get_pc_thunk.bp>:
    1215:	8b 2c 24             	mov    (%esp),%ebp
    1218:	c3                   	ret    

Disassembly of section .fini:

0000121c <_fini>:
    121c:	f3 0f 1e fb          	endbr32 
    1220:	53                   	push   %ebx
    1221:	83 ec 08             	sub    $0x8,%esp
    1224:	e8 67 fe ff ff       	call   1090 <__x86.get_pc_thunk.bx>
    1229:	81 c3 d7 2d 00 00    	add    $0x2dd7,%ebx
    122f:	83 c4 08             	add    $0x8,%esp
    1232:	5b                   	pop    %ebx
    1233:	c3                   	ret    
```
</details>


* les adresses virtuelles des labels main et _start sont respectivement : 0000119d et 00001060

## 2.1 Preprocessing

command :

```bash
gcc -E -Wall -m32 -I./include/ ./src/hello.c > ./build/misc/hello.i
```
command : 
```bash
cat ./build/misc/hello.i
```
<details>
<summary> result of cat ./build/misc/hello.i</summary>

```assembly
# 1 "./src/hello.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "./src/hello.c"
# 1 "/usr/include/stdio.h" 1 3 4
# 27 "/usr/include/stdio.h" 3 4
# 1 "/usr/include/bits/libc-header-start.h" 1 3 4
# 33 "/usr/include/bits/libc-header-start.h" 3 4
# 1 "/usr/include/features.h" 1 3 4
# 473 "/usr/include/features.h" 3 4
# 1 "/usr/include/sys/cdefs.h" 1 3 4
# 462 "/usr/include/sys/cdefs.h" 3 4
# 1 "/usr/include/bits/wordsize.h" 1 3 4
# 463 "/usr/include/sys/cdefs.h" 2 3 4
# 1 "/usr/include/bits/long-double.h" 1 3 4
# 464 "/usr/include/sys/cdefs.h" 2 3 4
# 474 "/usr/include/features.h" 2 3 4
# 497 "/usr/include/features.h" 3 4
# 1 "/usr/include/gnu/stubs.h" 1 3 4






# 1 "/usr/include/gnu/stubs-32.h" 1 3 4
# 8 "/usr/include/gnu/stubs.h" 2 3 4
# 498 "/usr/include/features.h" 2 3 4
# 34 "/usr/include/bits/libc-header-start.h" 2 3 4
# 28 "/usr/include/stdio.h" 2 3 4





# 1 "/usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/include/stddef.h" 1 3 4
# 209 "/usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/include/stddef.h" 3 4

# 209 "/usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/include/stddef.h" 3 4
typedef unsigned int size_t;
# 34 "/usr/include/stdio.h" 2 3 4


# 1 "/usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/include/stdarg.h" 1 3 4
# 40 "/usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/include/stdarg.h" 3 4
typedef __builtin_va_list __gnuc_va_list;
# 37 "/usr/include/stdio.h" 2 3 4

# 1 "/usr/include/bits/types.h" 1 3 4
# 27 "/usr/include/bits/types.h" 3 4
# 1 "/usr/include/bits/wordsize.h" 1 3 4
# 28 "/usr/include/bits/types.h" 2 3 4
# 1 "/usr/include/bits/timesize.h" 1 3 4
# 29 "/usr/include/bits/types.h" 2 3 4


typedef unsigned char __u_char;
typedef unsigned short int __u_short;
typedef unsigned int __u_int;
typedef unsigned long int __u_long;


typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef signed short int __int16_t;
typedef unsigned short int __uint16_t;
typedef signed int __int32_t;
typedef unsigned int __uint32_t;




__extension__ typedef signed long long int __int64_t;
__extension__ typedef unsigned long long int __uint64_t;



typedef __int8_t __int_least8_t;
typedef __uint8_t __uint_least8_t;
typedef __int16_t __int_least16_t;
typedef __uint16_t __uint_least16_t;
typedef __int32_t __int_least32_t;
typedef __uint32_t __uint_least32_t;
typedef __int64_t __int_least64_t;
typedef __uint64_t __uint_least64_t;






__extension__ typedef long long int __quad_t;
__extension__ typedef unsigned long long int __u_quad_t;







__extension__ typedef long long int __intmax_t;
__extension__ typedef unsigned long long int __uintmax_t;
# 141 "/usr/include/bits/types.h" 3 4
# 1 "/usr/include/bits/typesizes.h" 1 3 4
# 142 "/usr/include/bits/types.h" 2 3 4
# 1 "/usr/include/bits/time64.h" 1 3 4
# 143 "/usr/include/bits/types.h" 2 3 4


__extension__ typedef __uint64_t __dev_t;
__extension__ typedef unsigned int __uid_t;
__extension__ typedef unsigned int __gid_t;
__extension__ typedef unsigned long int __ino_t;
__extension__ typedef __uint64_t __ino64_t;
__extension__ typedef unsigned int __mode_t;
__extension__ typedef unsigned int __nlink_t;
__extension__ typedef long int __off_t;
__extension__ typedef __int64_t __off64_t;
__extension__ typedef int __pid_t;
__extension__ typedef struct { int __val[2]; } __fsid_t;
__extension__ typedef long int __clock_t;
__extension__ typedef unsigned long int __rlim_t;
__extension__ typedef __uint64_t __rlim64_t;
__extension__ typedef unsigned int __id_t;
__extension__ typedef long int __time_t;
__extension__ typedef unsigned int __useconds_t;
__extension__ typedef long int __suseconds_t;
__extension__ typedef __int64_t __suseconds64_t;

__extension__ typedef int __daddr_t;
__extension__ typedef int __key_t;


__extension__ typedef int __clockid_t;


__extension__ typedef void * __timer_t;


__extension__ typedef long int __blksize_t;




__extension__ typedef long int __blkcnt_t;
__extension__ typedef __int64_t __blkcnt64_t;


__extension__ typedef unsigned long int __fsblkcnt_t;
__extension__ typedef __uint64_t __fsblkcnt64_t;


__extension__ typedef unsigned long int __fsfilcnt_t;
__extension__ typedef __uint64_t __fsfilcnt64_t;


__extension__ typedef int __fsword_t;

__extension__ typedef int __ssize_t;


__extension__ typedef long int __syscall_slong_t;

__extension__ typedef unsigned long int __syscall_ulong_t;



typedef __off64_t __loff_t;
typedef char *__caddr_t;


__extension__ typedef int __intptr_t;


__extension__ typedef unsigned int __socklen_t;




typedef int __sig_atomic_t;







__extension__ typedef __int64_t __time64_t;
# 39 "/usr/include/stdio.h" 2 3 4
# 1 "/usr/include/bits/types/__fpos_t.h" 1 3 4




# 1 "/usr/include/bits/types/__mbstate_t.h" 1 3 4
# 13 "/usr/include/bits/types/__mbstate_t.h" 3 4
typedef struct
{
  int __count;
  union
  {
    unsigned int __wch;
    char __wchb[4];
  } __value;
} __mbstate_t;
# 6 "/usr/include/bits/types/__fpos_t.h" 2 3 4




typedef struct _G_fpos_t
{
  __off_t __pos;
  __mbstate_t __state;
} __fpos_t;
# 40 "/usr/include/stdio.h" 2 3 4
# 1 "/usr/include/bits/types/__fpos64_t.h" 1 3 4
# 10 "/usr/include/bits/types/__fpos64_t.h" 3 4
typedef struct _G_fpos64_t
{
  __off64_t __pos;
  __mbstate_t __state;
} __fpos64_t;
# 41 "/usr/include/stdio.h" 2 3 4
# 1 "/usr/include/bits/types/__FILE.h" 1 3 4



struct _IO_FILE;
typedef struct _IO_FILE __FILE;
# 42 "/usr/include/stdio.h" 2 3 4
# 1 "/usr/include/bits/types/FILE.h" 1 3 4



struct _IO_FILE;


typedef struct _IO_FILE FILE;
# 43 "/usr/include/stdio.h" 2 3 4
# 1 "/usr/include/bits/types/struct_FILE.h" 1 3 4
# 35 "/usr/include/bits/types/struct_FILE.h" 3 4
struct _IO_FILE;
struct _IO_marker;
struct _IO_codecvt;
struct _IO_wide_data;




typedef void _IO_lock_t;





struct _IO_FILE
{
  int _flags;


  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;


  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;

  struct _IO_marker *_markers;

  struct _IO_FILE *_chain;

  int _fileno;
  int _flags2;
  __off_t _old_offset;


  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];

  _IO_lock_t *_lock;







  __off64_t _offset;

  struct _IO_codecvt *_codecvt;
  struct _IO_wide_data *_wide_data;
  struct _IO_FILE *_freeres_list;
  void *_freeres_buf;
  size_t __pad5;
  int _mode;

  char _unused2[15 * sizeof (int) - 4 * sizeof (void *) - sizeof (size_t)];
};
# 44 "/usr/include/stdio.h" 2 3 4
# 52 "/usr/include/stdio.h" 3 4
typedef __gnuc_va_list va_list;
# 63 "/usr/include/stdio.h" 3 4
typedef __off_t off_t;
# 77 "/usr/include/stdio.h" 3 4
typedef __ssize_t ssize_t;






typedef __fpos_t fpos_t;
# 133 "/usr/include/stdio.h" 3 4
# 1 "/usr/include/bits/stdio_lim.h" 1 3 4
# 134 "/usr/include/stdio.h" 2 3 4



extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;






extern int remove (const char *__filename) __attribute__ ((__nothrow__ , __leaf__));

extern int rename (const char *__old, const char *__new) __attribute__ ((__nothrow__ , __leaf__));



extern int renameat (int __oldfd, const char *__old, int __newfd,
       const char *__new) __attribute__ ((__nothrow__ , __leaf__));
# 173 "/usr/include/stdio.h" 3 4
extern FILE *tmpfile (void) ;
# 187 "/usr/include/stdio.h" 3 4
extern char *tmpnam (char *__s) __attribute__ ((__nothrow__ , __leaf__)) ;




extern char *tmpnam_r (char *__s) __attribute__ ((__nothrow__ , __leaf__)) ;
# 204 "/usr/include/stdio.h" 3 4
extern char *tempnam (const char *__dir, const char *__pfx)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__malloc__)) ;







extern int fclose (FILE *__stream);




extern int fflush (FILE *__stream);
# 227 "/usr/include/stdio.h" 3 4
extern int fflush_unlocked (FILE *__stream);
# 246 "/usr/include/stdio.h" 3 4
extern FILE *fopen (const char *__restrict __filename,
      const char *__restrict __modes) ;




extern FILE *freopen (const char *__restrict __filename,
        const char *__restrict __modes,
        FILE *__restrict __stream) ;
# 279 "/usr/include/stdio.h" 3 4
extern FILE *fdopen (int __fd, const char *__modes) __attribute__ ((__nothrow__ , __leaf__)) ;
# 292 "/usr/include/stdio.h" 3 4
extern FILE *fmemopen (void *__s, size_t __len, const char *__modes)
  __attribute__ ((__nothrow__ , __leaf__)) ;




extern FILE *open_memstream (char **__bufloc, size_t *__sizeloc) __attribute__ ((__nothrow__ , __leaf__)) ;





extern void setbuf (FILE *__restrict __stream, char *__restrict __buf) __attribute__ ((__nothrow__ , __leaf__));



extern int setvbuf (FILE *__restrict __stream, char *__restrict __buf,
      int __modes, size_t __n) __attribute__ ((__nothrow__ , __leaf__));




extern void setbuffer (FILE *__restrict __stream, char *__restrict __buf,
         size_t __size) __attribute__ ((__nothrow__ , __leaf__));


extern void setlinebuf (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));







extern int fprintf (FILE *__restrict __stream,
      const char *__restrict __format, ...);




extern int printf (const char *__restrict __format, ...);

extern int sprintf (char *__restrict __s,
      const char *__restrict __format, ...) __attribute__ ((__nothrow__));





extern int vfprintf (FILE *__restrict __s, const char *__restrict __format,
       __gnuc_va_list __arg);




extern int vprintf (const char *__restrict __format, __gnuc_va_list __arg);

extern int vsprintf (char *__restrict __s, const char *__restrict __format,
       __gnuc_va_list __arg) __attribute__ ((__nothrow__));



extern int snprintf (char *__restrict __s, size_t __maxlen,
       const char *__restrict __format, ...)
     __attribute__ ((__nothrow__)) __attribute__ ((__format__ (__printf__, 3, 4)));

extern int vsnprintf (char *__restrict __s, size_t __maxlen,
        const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__nothrow__)) __attribute__ ((__format__ (__printf__, 3, 0)));
# 379 "/usr/include/stdio.h" 3 4
extern int vdprintf (int __fd, const char *__restrict __fmt,
       __gnuc_va_list __arg)
     __attribute__ ((__format__ (__printf__, 2, 0)));
extern int dprintf (int __fd, const char *__restrict __fmt, ...)
     __attribute__ ((__format__ (__printf__, 2, 3)));







extern int fscanf (FILE *__restrict __stream,
     const char *__restrict __format, ...) ;




extern int scanf (const char *__restrict __format, ...) ;

extern int sscanf (const char *__restrict __s,
     const char *__restrict __format, ...) __attribute__ ((__nothrow__ , __leaf__));





# 1 "/usr/include/bits/floatn.h" 1 3 4
# 119 "/usr/include/bits/floatn.h" 3 4
# 1 "/usr/include/bits/floatn-common.h" 1 3 4
# 24 "/usr/include/bits/floatn-common.h" 3 4
# 1 "/usr/include/bits/long-double.h" 1 3 4
# 25 "/usr/include/bits/floatn-common.h" 2 3 4
# 120 "/usr/include/bits/floatn.h" 2 3 4
# 407 "/usr/include/stdio.h" 2 3 4



extern int fscanf (FILE *__restrict __stream, const char *__restrict __format, ...) __asm__ ("" "__isoc99_fscanf")

                               ;
extern int scanf (const char *__restrict __format, ...) __asm__ ("" "__isoc99_scanf")
                              ;
extern int sscanf (const char *__restrict __s, const char *__restrict __format, ...) __asm__ ("" "__isoc99_sscanf") __attribute__ ((__nothrow__ , __leaf__))

                      ;
# 435 "/usr/include/stdio.h" 3 4
extern int vfscanf (FILE *__restrict __s, const char *__restrict __format,
      __gnuc_va_list __arg)
     __attribute__ ((__format__ (__scanf__, 2, 0))) ;





extern int vscanf (const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__format__ (__scanf__, 1, 0))) ;


extern int vsscanf (const char *__restrict __s,
      const char *__restrict __format, __gnuc_va_list __arg)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__format__ (__scanf__, 2, 0)));





extern int vfscanf (FILE *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vfscanf")



     __attribute__ ((__format__ (__scanf__, 2, 0))) ;
extern int vscanf (const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vscanf")

     __attribute__ ((__format__ (__scanf__, 1, 0))) ;
extern int vsscanf (const char *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg) __asm__ ("" "__isoc99_vsscanf") __attribute__ ((__nothrow__ , __leaf__))



     __attribute__ ((__format__ (__scanf__, 2, 0)));
# 489 "/usr/include/stdio.h" 3 4
extern int fgetc (FILE *__stream);
extern int getc (FILE *__stream);





extern int getchar (void);






extern int getc_unlocked (FILE *__stream);
extern int getchar_unlocked (void);
# 514 "/usr/include/stdio.h" 3 4
extern int fgetc_unlocked (FILE *__stream);
# 525 "/usr/include/stdio.h" 3 4
extern int fputc (int __c, FILE *__stream);
extern int putc (int __c, FILE *__stream);





extern int putchar (int __c);
# 541 "/usr/include/stdio.h" 3 4
extern int fputc_unlocked (int __c, FILE *__stream);







extern int putc_unlocked (int __c, FILE *__stream);
extern int putchar_unlocked (int __c);






extern int getw (FILE *__stream);


extern int putw (int __w, FILE *__stream);







extern char *fgets (char *__restrict __s, int __n, FILE *__restrict __stream)
     __attribute__ ((__access__ (__write_only__, 1, 2)));
# 608 "/usr/include/stdio.h" 3 4
extern __ssize_t __getdelim (char **__restrict __lineptr,
                             size_t *__restrict __n, int __delimiter,
                             FILE *__restrict __stream) ;
extern __ssize_t getdelim (char **__restrict __lineptr,
                           size_t *__restrict __n, int __delimiter,
                           FILE *__restrict __stream) ;







extern __ssize_t getline (char **__restrict __lineptr,
                          size_t *__restrict __n,
                          FILE *__restrict __stream) ;







extern int fputs (const char *__restrict __s, FILE *__restrict __stream);





extern int puts (const char *__s);






extern int ungetc (int __c, FILE *__stream);






extern size_t fread (void *__restrict __ptr, size_t __size,
       size_t __n, FILE *__restrict __stream) ;




extern size_t fwrite (const void *__restrict __ptr, size_t __size,
        size_t __n, FILE *__restrict __s);
# 678 "/usr/include/stdio.h" 3 4
extern size_t fread_unlocked (void *__restrict __ptr, size_t __size,
         size_t __n, FILE *__restrict __stream) ;
extern size_t fwrite_unlocked (const void *__restrict __ptr, size_t __size,
          size_t __n, FILE *__restrict __stream);







extern int fseek (FILE *__stream, long int __off, int __whence);




extern long int ftell (FILE *__stream) ;




extern void rewind (FILE *__stream);
# 712 "/usr/include/stdio.h" 3 4
extern int fseeko (FILE *__stream, __off_t __off, int __whence);




extern __off_t ftello (FILE *__stream) ;
# 736 "/usr/include/stdio.h" 3 4
extern int fgetpos (FILE *__restrict __stream, fpos_t *__restrict __pos);




extern int fsetpos (FILE *__stream, const fpos_t *__pos);
# 762 "/usr/include/stdio.h" 3 4
extern void clearerr (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));

extern int feof (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;

extern int ferror (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;



extern void clearerr_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));
extern int feof_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
extern int ferror_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;







extern void perror (const char *__s);




extern int fileno (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;




extern int fileno_unlocked (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;
# 799 "/usr/include/stdio.h" 3 4
extern FILE *popen (const char *__command, const char *__modes) ;





extern int pclose (FILE *__stream);





extern char *ctermid (char *__s) __attribute__ ((__nothrow__ , __leaf__));
# 839 "/usr/include/stdio.h" 3 4
extern void flockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));



extern int ftrylockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__)) ;


extern void funlockfile (FILE *__stream) __attribute__ ((__nothrow__ , __leaf__));
# 857 "/usr/include/stdio.h" 3 4
extern int __uflow (FILE *);
extern int __overflow (FILE *, int);
# 874 "/usr/include/stdio.h" 3 4

# 2 "./src/hello.c" 2
# 1 "./include/hello.h" 1
# 3 "./src/hello.c" 2


# 4 "./src/hello.c"
int main(void)
{




 while(1);

return 0;
}
```
</details>


Le processeur se charge de traiter les macro. En effet, toutes les librairies necessaires on etet ajoutees. De plus le #ifdef n existe plus. La valeur de PRINT_HELLO etant a 1, la commande printf est presente dans le code apres le preprocessing.

command : 
```bash
sed -i 's/1/0/g' ./include/hello.h
```
* #include permet d inclure une librairie

* #define permet de definir une variable et pototielement lui assigner une valeur

* #undef annule l action de #define sur une variable

* #ifdef permet de verifier si une variable est definie

* #ifndef permet de verifier si une variable n est pas definie

* #if permet de verifier une expression logique

* #elif permet de verifier une autre expression logique si celle d avant est fausse

* #else est preprocessee si les expression logiques precedentes sont toutes fausses

* #endif permet de borner le scope d un #if, #ifdef ...

## 2.2 Analyse et generation de code natif

command : 
```bash
gcc -S -Wall -m32 ./build/misc/hello.i -o ./build/misc/hello.s
```
command : 
```bash
cat ./build/misc/hello.s
```
<details>
<summary> result of cat ./build/misc/hello.s</summary>

```assembly
	.file	"hello.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	call	__x86.get_pc_thunk.ax
	addl	$_GLOBAL_OFFSET_TABLE_, %eax
.L2:
	jmp	.L2
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
	.globl	__x86.get_pc_thunk.ax
	.hidden	__x86.get_pc_thunk.ax
	.type	__x86.get_pc_thunk.ax, @function
__x86.get_pc_thunk.ax:
.LFB1:
	.cfi_startproc
	movl	(%esp), %eax
	ret
	.cfi_endproc
.LFE1:
	.ident	"GCC: (GNU) 10.2.0"
	.section	.note.GNU-stack,"",@progbits
```
</details>


le code preprocesse est traduit en assembleur

command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -Wall -m32 ./build/misc/hello.i -o ./build/misc/hello.s
```
![](./assembleur.png)

* un label est un pointeur vers une adresse memoire. Le label main representera le debut de la fonction main

* c'est une operation elementaire a effectuer

* un registre est une tres petite zone memoire permetant de stocker le resultat d une operation avant de la mettre dans le cache puis dans la ram.

## 2.3 Assemblage

command : 
```bash
as --32 ./build/misc/hello.s -o ./build/obj/hello.o
```
command : 
```bash
objdump -S ./build/obj/hello.o
```
<details>
<summary> result of objdump -S ./build/obj/hello.o</summary>

```assembly

./build/obj/hello.o:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
   0:	55                   	push   %ebp
   1:	89 e5                	mov    %esp,%ebp
   3:	e8 fc ff ff ff       	call   4 <main+0x4>
   8:	05 01 00 00 00       	add    $0x1,%eax
   d:	eb fe                	jmp    d <main+0xd>

Disassembly of section .text.__x86.get_pc_thunk.ax:

00000000 <__x86.get_pc_thunk.ax>:
   0:	8b 04 24             	mov    (%esp),%eax
   3:	c3                   	ret    
```
</details>


* adresse relative du main : 00000000

command : 
```bash
readelf -h ./build/obj/hello.o
```
<details>
<summary> result of readelf -h ./build/obj/hello.o</summary>

```assembly
ELF Header:
  Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF32
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              REL (Relocatable file)
  Machine:                           Intel 80386
  Version:                           0x1
  Entry point address:               0x0
  Start of program headers:          0 (bytes into file)
  Start of section headers:          424 (bytes into file)
  Flags:                             0x0
  Size of this header:               52 (bytes)
  Size of program headers:           0 (bytes)
  Number of program headers:         0
  Size of section headers:           40 (bytes)
  Number of section headers:         13
  Section header string table index: 12
```
</details>


* architecture CPU cible : Intel 80386

* type de fichier binaire : ELF32 REL, c est donc un fichier de type Relocatable file, ceci permet de linker le fichier avec d autres objets pour creer un executable ou de creer un objet partagee

* hello.o n est pas un executable car c'est un fichier de type Relocatable

## 2.4 Edition des liens

command : 
```bash
gcc -fno-asynchronous-unwind-tables -Wall -m32 ./build/obj/hello.o -o ./build/misc/hello.s
```
command : 
```bash
./build/bin/hello
```
command : 
```bash
readelf -h ./build/bin/hello
```
<details>
<summary> result of readelf -h ./build/bin/hello</summary>

```assembly
ELF Header:
  Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF32
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              DYN (Shared object file)
  Machine:                           Intel 80386
  Version:                           0x1
  Entry point address:               0x1050
  Start of program headers:          52 (bytes into file)
  Start of section headers:          13944 (bytes into file)
  Flags:                             0x0
  Size of this header:               52 (bytes)
  Size of program headers:           32 (bytes)
  Number of program headers:         12
  Size of section headers:           40 (bytes)
  Number of section headers:         30
  Section header string table index: 29
```
</details>


* type de fichier binaire : ELF32 DYN (Shared object file) 

* adresse entree programme : 0x1050

command : 
```bash
objdump -S ./build/bin/hello
```
<details>
<summary> result of objdump -S ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386


Disassembly of section .init:

00001000 <_init>:
    1000:	f3 0f 1e fb          	endbr32 
    1004:	53                   	push   %ebx
    1005:	83 ec 08             	sub    $0x8,%esp
    1008:	e8 83 00 00 00       	call   1090 <__x86.get_pc_thunk.bx>
    100d:	81 c3 f3 2f 00 00    	add    $0x2ff3,%ebx
    1013:	8b 83 f4 ff ff ff    	mov    -0xc(%ebx),%eax
    1019:	85 c0                	test   %eax,%eax
    101b:	74 02                	je     101f <_init+0x1f>
    101d:	ff d0                	call   *%eax
    101f:	83 c4 08             	add    $0x8,%esp
    1022:	5b                   	pop    %ebx
    1023:	c3                   	ret    

Disassembly of section .plt:

00001030 <__libc_start_main@plt-0x10>:
    1030:	ff b3 04 00 00 00    	push   0x4(%ebx)
    1036:	ff a3 08 00 00 00    	jmp    *0x8(%ebx)
    103c:	00 00                	add    %al,(%eax)
	...

00001040 <__libc_start_main@plt>:
    1040:	ff a3 0c 00 00 00    	jmp    *0xc(%ebx)
    1046:	68 00 00 00 00       	push   $0x0
    104b:	e9 e0 ff ff ff       	jmp    1030 <_init+0x30>

Disassembly of section .text:

00001050 <_start>:
    1050:	f3 0f 1e fb          	endbr32 
    1054:	31 ed                	xor    %ebp,%ebp
    1056:	5e                   	pop    %esi
    1057:	89 e1                	mov    %esp,%ecx
    1059:	83 e4 f0             	and    $0xfffffff0,%esp
    105c:	50                   	push   %eax
    105d:	54                   	push   %esp
    105e:	52                   	push   %edx
    105f:	e8 22 00 00 00       	call   1086 <_start+0x36>
    1064:	81 c3 9c 2f 00 00    	add    $0x2f9c,%ebx
    106a:	8d 83 10 d2 ff ff    	lea    -0x2df0(%ebx),%eax
    1070:	50                   	push   %eax
    1071:	8d 83 a0 d1 ff ff    	lea    -0x2e60(%ebx),%eax
    1077:	50                   	push   %eax
    1078:	51                   	push   %ecx
    1079:	56                   	push   %esi
    107a:	ff b3 f8 ff ff ff    	push   -0x8(%ebx)
    1080:	e8 bb ff ff ff       	call   1040 <__libc_start_main@plt>
    1085:	f4                   	hlt    
    1086:	8b 1c 24             	mov    (%esp),%ebx
    1089:	c3                   	ret    
    108a:	66 90                	xchg   %ax,%ax
    108c:	66 90                	xchg   %ax,%ax
    108e:	66 90                	xchg   %ax,%ax

00001090 <__x86.get_pc_thunk.bx>:
    1090:	8b 1c 24             	mov    (%esp),%ebx
    1093:	c3                   	ret    
    1094:	66 90                	xchg   %ax,%ax
    1096:	66 90                	xchg   %ax,%ax
    1098:	66 90                	xchg   %ax,%ax
    109a:	66 90                	xchg   %ax,%ax
    109c:	66 90                	xchg   %ax,%ax
    109e:	66 90                	xchg   %ax,%ax

000010a0 <deregister_tm_clones>:
    10a0:	e8 e4 00 00 00       	call   1189 <__x86.get_pc_thunk.dx>
    10a5:	81 c2 5b 2f 00 00    	add    $0x2f5b,%edx
    10ab:	8d 8a 18 00 00 00    	lea    0x18(%edx),%ecx
    10b1:	8d 82 18 00 00 00    	lea    0x18(%edx),%eax
    10b7:	39 c8                	cmp    %ecx,%eax
    10b9:	74 1d                	je     10d8 <deregister_tm_clones+0x38>
    10bb:	8b 82 ec ff ff ff    	mov    -0x14(%edx),%eax
    10c1:	85 c0                	test   %eax,%eax
    10c3:	74 13                	je     10d8 <deregister_tm_clones+0x38>
    10c5:	55                   	push   %ebp
    10c6:	89 e5                	mov    %esp,%ebp
    10c8:	83 ec 14             	sub    $0x14,%esp
    10cb:	51                   	push   %ecx
    10cc:	ff d0                	call   *%eax
    10ce:	83 c4 10             	add    $0x10,%esp
    10d1:	c9                   	leave  
    10d2:	c3                   	ret    
    10d3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    10d7:	90                   	nop
    10d8:	c3                   	ret    
    10d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

000010e0 <register_tm_clones>:
    10e0:	e8 a4 00 00 00       	call   1189 <__x86.get_pc_thunk.dx>
    10e5:	81 c2 1b 2f 00 00    	add    $0x2f1b,%edx
    10eb:	55                   	push   %ebp
    10ec:	89 e5                	mov    %esp,%ebp
    10ee:	53                   	push   %ebx
    10ef:	8d 8a 18 00 00 00    	lea    0x18(%edx),%ecx
    10f5:	8d 82 18 00 00 00    	lea    0x18(%edx),%eax
    10fb:	83 ec 04             	sub    $0x4,%esp
    10fe:	29 c8                	sub    %ecx,%eax
    1100:	89 c3                	mov    %eax,%ebx
    1102:	c1 e8 1f             	shr    $0x1f,%eax
    1105:	c1 fb 02             	sar    $0x2,%ebx
    1108:	01 d8                	add    %ebx,%eax
    110a:	d1 f8                	sar    %eax
    110c:	74 14                	je     1122 <register_tm_clones+0x42>
    110e:	8b 92 fc ff ff ff    	mov    -0x4(%edx),%edx
    1114:	85 d2                	test   %edx,%edx
    1116:	74 0a                	je     1122 <register_tm_clones+0x42>
    1118:	83 ec 08             	sub    $0x8,%esp
    111b:	50                   	push   %eax
    111c:	51                   	push   %ecx
    111d:	ff d2                	call   *%edx
    111f:	83 c4 10             	add    $0x10,%esp
    1122:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    1125:	c9                   	leave  
    1126:	c3                   	ret    
    1127:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    112e:	66 90                	xchg   %ax,%ax

00001130 <__do_global_dtors_aux>:
    1130:	f3 0f 1e fb          	endbr32 
    1134:	55                   	push   %ebp
    1135:	89 e5                	mov    %esp,%ebp
    1137:	53                   	push   %ebx
    1138:	e8 53 ff ff ff       	call   1090 <__x86.get_pc_thunk.bx>
    113d:	81 c3 c3 2e 00 00    	add    $0x2ec3,%ebx
    1143:	83 ec 04             	sub    $0x4,%esp
    1146:	80 bb 18 00 00 00 00 	cmpb   $0x0,0x18(%ebx)
    114d:	75 28                	jne    1177 <__do_global_dtors_aux+0x47>
    114f:	8b 83 f0 ff ff ff    	mov    -0x10(%ebx),%eax
    1155:	85 c0                	test   %eax,%eax
    1157:	74 12                	je     116b <__do_global_dtors_aux+0x3b>
    1159:	83 ec 0c             	sub    $0xc,%esp
    115c:	ff b3 14 00 00 00    	push   0x14(%ebx)
    1162:	ff 93 f0 ff ff ff    	call   *-0x10(%ebx)
    1168:	83 c4 10             	add    $0x10,%esp
    116b:	e8 30 ff ff ff       	call   10a0 <deregister_tm_clones>
    1170:	c6 83 18 00 00 00 01 	movb   $0x1,0x18(%ebx)
    1177:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    117a:	c9                   	leave  
    117b:	c3                   	ret    
    117c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00001180 <frame_dummy>:
    1180:	f3 0f 1e fb          	endbr32 
    1184:	e9 57 ff ff ff       	jmp    10e0 <register_tm_clones>

00001189 <__x86.get_pc_thunk.dx>:
    1189:	8b 14 24             	mov    (%esp),%edx
    118c:	c3                   	ret    

0000118d <main>:
    118d:	55                   	push   %ebp
    118e:	89 e5                	mov    %esp,%ebp
    1190:	e8 07 00 00 00       	call   119c <__x86.get_pc_thunk.ax>
    1195:	05 6b 2e 00 00       	add    $0x2e6b,%eax
    119a:	eb fe                	jmp    119a <main+0xd>

0000119c <__x86.get_pc_thunk.ax>:
    119c:	8b 04 24             	mov    (%esp),%eax
    119f:	c3                   	ret    

000011a0 <__libc_csu_init>:
    11a0:	f3 0f 1e fb          	endbr32 
    11a4:	55                   	push   %ebp
    11a5:	e8 6b 00 00 00       	call   1215 <__x86.get_pc_thunk.bp>
    11aa:	81 c5 56 2e 00 00    	add    $0x2e56,%ebp
    11b0:	57                   	push   %edi
    11b1:	56                   	push   %esi
    11b2:	53                   	push   %ebx
    11b3:	83 ec 0c             	sub    $0xc,%esp
    11b6:	89 eb                	mov    %ebp,%ebx
    11b8:	8b 7c 24 28          	mov    0x28(%esp),%edi
    11bc:	e8 3f fe ff ff       	call   1000 <_init>
    11c1:	8d 9d f8 fe ff ff    	lea    -0x108(%ebp),%ebx
    11c7:	8d 85 f4 fe ff ff    	lea    -0x10c(%ebp),%eax
    11cd:	29 c3                	sub    %eax,%ebx
    11cf:	c1 fb 02             	sar    $0x2,%ebx
    11d2:	74 29                	je     11fd <__libc_csu_init+0x5d>
    11d4:	31 f6                	xor    %esi,%esi
    11d6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    11dd:	8d 76 00             	lea    0x0(%esi),%esi
    11e0:	83 ec 04             	sub    $0x4,%esp
    11e3:	57                   	push   %edi
    11e4:	ff 74 24 2c          	push   0x2c(%esp)
    11e8:	ff 74 24 2c          	push   0x2c(%esp)
    11ec:	ff 94 b5 f4 fe ff ff 	call   *-0x10c(%ebp,%esi,4)
    11f3:	83 c6 01             	add    $0x1,%esi
    11f6:	83 c4 10             	add    $0x10,%esp
    11f9:	39 f3                	cmp    %esi,%ebx
    11fb:	75 e3                	jne    11e0 <__libc_csu_init+0x40>
    11fd:	83 c4 0c             	add    $0xc,%esp
    1200:	5b                   	pop    %ebx
    1201:	5e                   	pop    %esi
    1202:	5f                   	pop    %edi
    1203:	5d                   	pop    %ebp
    1204:	c3                   	ret    
    1205:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    120c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

00001210 <__libc_csu_fini>:
    1210:	f3 0f 1e fb          	endbr32 
    1214:	c3                   	ret    

00001215 <__x86.get_pc_thunk.bp>:
    1215:	8b 2c 24             	mov    (%esp),%ebp
    1218:	c3                   	ret    

Disassembly of section .fini:

0000121c <_fini>:
    121c:	f3 0f 1e fb          	endbr32 
    1220:	53                   	push   %ebx
    1221:	83 ec 08             	sub    $0x8,%esp
    1224:	e8 67 fe ff ff       	call   1090 <__x86.get_pc_thunk.bx>
    1229:	81 c3 d7 2d 00 00    	add    $0x2dd7,%ebx
    122f:	83 c4 08             	add    $0x8,%esp
    1232:	5b                   	pop    %ebx
    1233:	c3                   	ret    
```
</details>


* adresse relative fonction main : 1189

* adresse de _start :  1050

* hello n est toujours pas un executable

command : 
```bash
./build/bin/hello
```
## 2.5 Startup file

command : 
```bash
ls -lh build/bin
```
<details>
<summary> result of ls -lh build/bin</summary>

```assembly
total 16K
-rwxr-xr-x 1 paul paul 15K May 26 02:02 hello
```
</details>


* taille de l exe : 15k

command : 
```bash
gcc -v -fno-asynchronous-unwind-tables -Wall -m32 ./build/obj/hello.o -o ./build/bin/hello
```
command : 
```bash
as --32 ./build/startup/crt0.s -o ./build/obj/crt0.o
```
command : 
```bash
ld -melf_i386 ./build/obj/crt0.o ./build/obj/hello.o -o ./build/bin/hello
```
command : 
```bash
objdump -S ./build/bin/hello
```
<details>
<summary> result of objdump -S ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386


Disassembly of section .text:

08049000 <_start>:
 8049000:	55                   	push   %ebp
 8049001:	89 e5                	mov    %esp,%ebp
 8049003:	e8 0c 00 00 00       	call   8049014 <main>
 8049008:	b8 01 00 00 00       	mov    $0x1,%eax
 804900d:	bb 00 00 00 00       	mov    $0x0,%ebx
 8049012:	cd 80                	int    $0x80

08049014 <main>:
 8049014:	55                   	push   %ebp
 8049015:	89 e5                	mov    %esp,%ebp
 8049017:	e8 07 00 00 00       	call   8049023 <__x86.get_pc_thunk.ax>
 804901c:	05 e4 0f 00 00       	add    $0xfe4,%eax
 8049021:	eb fe                	jmp    8049021 <main+0xd>

08049023 <__x86.get_pc_thunk.ax>:
 8049023:	8b 04 24             	mov    (%esp),%eax
 8049026:	c3                   	ret    
```
</details>


* Le fichier dessasemblee est beaucoup plus petit, il n y a plus que _start, main et __x86.get_pc_thunk.ax

* adresse de main : 8049014

* adresse de _start : 8049000

command : 
```bash
ls -l build/bin
```
<details>
<summary> result of ls -l build/bin</summary>

```assembly
total 12
-rwxr-xr-x 1 paul paul 8856 May 26 02:02 hello
```
</details>


* taille : 9 k

command : 
```bash
readelf -h ./build/bin/hello
```
<details>
<summary> result of readelf -h ./build/bin/hello</summary>

```assembly
ELF Header:
  Magic:   7f 45 4c 46 01 01 01 00 00 00 00 00 00 00 00 00 
  Class:                             ELF32
  Data:                              2's complement, little endian
  Version:                           1 (current)
  OS/ABI:                            UNIX - System V
  ABI Version:                       0
  Type:                              EXEC (Executable file)
  Machine:                           Intel 80386
  Version:                           0x1
  Entry point address:               0x8049000
  Start of program headers:          52 (bytes into file)
  Start of section headers:          8536 (bytes into file)
  Flags:                             0x0
  Size of this header:               52 (bytes)
  Size of program headers:           32 (bytes)
  Number of program headers:         6
  Size of section headers:           40 (bytes)
  Number of section headers:         8
  Section header string table index: 7
```
</details>


* hello est maintenant un executable

command : 
```bash
./build/bin/hello
```
## 2.6 Linker

command : 
```bash
gcc -m32 -Wl,--verbose
```
command : 
```bash
objdump -h ./build/bin/hello
```
<details>
<summary> result of objdump -h ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386

Sections:
Idx Name          Size      VMA       LMA       File off  Algn
  0 .note.gnu.property 00000028  080480f4  080480f4  000000f4  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .text         00000027  08049000  08049000  00001000  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
  2 .got.plt      0000000c  0804a000  0804a000  00002000  2**2
                  CONTENTS, ALLOC, LOAD, DATA
  3 .comment      00000012  00000000  00000000  0000200c  2**0
                  CONTENTS, READONLY
```
</details>


* On observe 4 sections

0. .note.gnu.property, longueur : 28

1. .text, longueur : 27

2. .got.plt : c

3. .comment, longueur : 12

command : 
```bash
objdump -S ./build/bin/hello
```
<details>
<summary> result of objdump -S ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386


Disassembly of section .text:

08049000 <_start>:
 8049000:	55                   	push   %ebp
 8049001:	89 e5                	mov    %esp,%ebp
 8049003:	e8 0c 00 00 00       	call   8049014 <main>
 8049008:	b8 01 00 00 00       	mov    $0x1,%eax
 804900d:	bb 00 00 00 00       	mov    $0x0,%ebx
 8049012:	cd 80                	int    $0x80

08049014 <main>:
 8049014:	55                   	push   %ebp
 8049015:	89 e5                	mov    %esp,%ebp
 8049017:	e8 07 00 00 00       	call   8049023 <__x86.get_pc_thunk.ax>
 804901c:	05 e4 0f 00 00       	add    $0xfe4,%eax
 8049021:	eb fe                	jmp    8049021 <main+0xd>

08049023 <__x86.get_pc_thunk.ax>:
 8049023:	8b 04 24             	mov    (%esp),%eax
 8049026:	c3                   	ret    
```
</details>


command : 
```bash
objdump -s ./build/bin/hello
```
<details>
<summary> result of objdump -s ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386

Contents of section .note.gnu.property:
 80480f4 04000000 18000000 05000000 474e5500  ............GNU.
 8048104 010001c0 04000000 01000000 020001c0  ................
 8048114 04000000 00000000                    ........        
Contents of section .text:
 8049000 5589e5e8 0c000000 b8010000 00bb0000  U...............
 8049010 0000cd80 5589e5e8 07000000 05e40f00  ....U...........
 8049020 00ebfe8b 0424c3                      .....$.         
Contents of section .got.plt:
 804a000 00000000 00000000 00000000           ............    
Contents of section .comment:
 0000 4743433a 2028474e 55292031 302e322e  GCC: (GNU) 10.2.
 0010 3000                                 0.              
```
</details>


* dans comment, il y a la version du compilateur, il  y a 4 sections

command : 
```bash
cat ./build/script/linker_script_minimal.ld
```
<details>
<summary> result of cat ./build/script/linker_script_minimal.ld</summary>

```assembly
OUTPUT_FORMAT("elf32-i386")
OUTPUT_ARCH(i386)
ENTRY(_start)

SECTIONS
{
    . = SEGMENT_START("text-segment", 0x08048000) + SIZEOF_HEADERS;
	.text :
    {
        *(.text)
    }
    .rodata : 
	{ 
        *(.rodata) 
    }
    .data :
    {
        *(.data)
    }
    .bss :
    {
        *(.bss)
    }
    /DISCARD/ : 
	{ 
        *(.comment)
        *(.note.GNU-stack)
        *(.eh_frame)
    }
}

```
</details>


command : 
```bash
gcc -c -fno-asynchronous-unwind-tables -Wall -m32 -I./include/ ./src/hello_var.c -o ./build/obj/hello.o
```
command : 
```bash
ld -melf_i386 -T ./build/script/linker_script_minimal.ld ./build/obj/crt0.o ./build/obj/hello.o -o ./build/bin/hello
```
command : 
```bash
objdump -h ./build/bin/hello
```
<details>
<summary> result of objdump -h ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386

Sections:
Idx Name          Size      VMA       LMA       File off  Algn
  0 .text         00000023  080480d4  080480d4  000000d4  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
  1 .text.__x86.get_pc_thunk.ax 00000004  080480f7  080480f7  000000f7  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
  2 .rodata       00000004  080480fc  080480fc  000000fc  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .note.gnu.property 00000028  08048100  08048100  00000100  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  4 .data         00000004  08048128  08048128  00000128  2**2
                  CONTENTS, ALLOC, LOAD, DATA
  5 .got.plt      0000000c  0804812c  0804812c  0000012c  2**2
                  CONTENTS, ALLOC, LOAD, DATA
  6 .bss          00000004  08048138  08048138  00000138  2**2
                  ALLOC
```
</details>


Il y a 2 sections en plus, une pour les variables (data) et une pour les constantes(rodata), c'est donc coherent.

command : 
```bash
objdump -s ./build/bin/hello
```
<details>
<summary> result of objdump -s ./build/bin/hello</summary>

```assembly

./build/bin/hello:     file format elf32-i386

Contents of section .text:
 80480d4 5589e5e8 0c000000 b8010000 00bb0000  U...............
 80480e4 0000cd80 5589e5e8 07000000 053c0000  ....U........<..
 80480f4 00ebfe                               ...             
Contents of section .text.__x86.get_pc_thunk.ax:
 80480f7 8b0424c3                             ..$.            
Contents of section .rodata:
 80480fc 02000000                             ....            
Contents of section .note.gnu.property:
 8048100 04000000 18000000 05000000 474e5500  ............GNU.
 8048110 010001c0 04000000 01000000 020001c0  ................
 8048120 04000000 00000000                    ........        
Contents of section .data:
 8048128 01000000                             ....            
Contents of section .got.plt:
 804812c 00000000 00000000 00000000           ............    
```
</details>


* la variable a est dans data

* taille elf

command : 
```bash
ls -lh build/bin
```
<details>
<summary> result of ls -lh build/bin</summary>

```assembly
total 4.0K
-rwxr-xr-x 1 paul paul 1.1K May 26 02:02 hello
```
</details>


* l executable fait maintenant 1,1k

command : 
```bash
strip ./build/bin/hello
```
command : 
```bash
ls -lh build/bin
```
<details>
<summary> result of ls -lh build/bin</summary>

```assembly
total 4.0K
-rwxr-xr-x 1 paul paul 764 May 26 02:02 hello
```
</details>


* l executable fait maintenant 764

* Le fichier hello est toujours un executable

command : 
```bash
./build/bin/hello
```
## 2.7 Execution et segmentation /!\ la boucle while est presente depuis le debut du TP

command : 
```bash
gcc -c -fno-asynchronous-unwind-tables -Wall -m32 -I./include/ ./src/hello.c -o ./build/obj/hello.o
```
command : 
```bash
ld -melf_i386 -T ./build/script/linker_script_minimal.ld ./build/obj/crt0.o ./build/obj/hello.o -o ./build/bin/hello
```
command : 
```bash
strip ./build/bin/hello
```
<details>
<summary> result of strip ./build/bin/hello</summary>

```assembly
```
</details>


command : 
```bash
./build/bin/hello
```
### recup pid

command : 
```bash
ps -a | cut -d "delimiter" -f (2)
```
<details>
<summary> result of $ps -a | grep -i hello | cut -d " " -f 3</summary>

```assembly
73222
```
</details>

command :
```bash
cat /proc/73222/maps
```
<details>
<summary>result ofcat /proc/73222/maps </summary>

```bash

08048000-08049000 rwxp 00000000 103:04 14185366                          /home/paul/labo/ensi/disco/toolchain/build/bin/hello
f7fca000-f7fce000 r--p 00000000 00:00 0                                  [vvar]
f7fce000-f7fd0000 r-xp 00000000 00:00 0                                  [vdso]
ffa83000-ffaa4000 rwxp 00000000 00:00 0                                  [stack]
```
</summary>





Paul JACOBS