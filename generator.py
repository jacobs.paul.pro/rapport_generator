import subprocess
import sys
import os

global project_path


project_path = "../disco/except"

def run_ghost_command(command):
    print("cd "+ project_path+" && " + command)
    os.system("cd "+ project_path+" && " + command)

def not_run_command(command, out):
    print("cd "+ project_path+" && " + command)
    out.write("command : \n")
    out.write("```bash\n")
    out.write(command +"\n")
    out.write("```\n")

def run_command(command, out):
    print("cd "+ project_path+" && " + command)
    out.write("command : \n")
    out.write("```bash\n")
    out.write(command +"\n")
    out.write("```\n")
    # subprocess.Popen("ls -R", cwd="/home/paul/labo/ensi/disco/toolchain", shell=True)
    # p = subprocess.Popen(command.split(' '), cwd="/home/paul/labo/ensi/disco/toolchain", shell=True)
    # os.waitpid(p.pid, 0)
    os.system("cd "+ project_path+" && " + command)


def check_command_output(command, out):
    print(command)
    out.write("command : \n")
    out.write("```bash\n")
    out.write(command+"\n")
    out.write("```\n")
    out.write("<details>\n")
    out.write("<summary> result of " + command + "</summary>\n\n")
    out.write("```assembly\n" )
    out.write(subprocess.check_output(command.split(' '), cwd=project_path).decode(sys.stdout.encoding))
    out.write("```\n")
    out.write("</details>\n\n\n")

def write_title(text, out):
    out.write(text+"\n")

def write_text(text, out):
    out.write(text)


def translate_line(line, out):
    c = line[0]
    if c == '$':
        check_command_output(line[1:].rstrip(), out)
    elif c == '%':
        run_command(line[1:].rstrip(), out)
    elif c == '!':
        not_run_command(line[1:].rstrip(), out)
    elif c == '&':
        run_ghost_command(line[1:].rstrip())
    elif c == '#' or c == '*':
        write_title(line, out)
    else:
        write_text(line, out)



with open("tp6/tp6_2.rapport", 'r') as f:
    command_count=1
    with open("tp6/res_2.md", 'w') as out:
        # while (l = f.readline()) :
        for l in f:
            if l.rstrip() == "end":
                exit()
            if l.rstrip() == "":
                continue
            print(command_count)
            command_count+=2
            translate_line(l,  out)
            print("\n\n\n")
