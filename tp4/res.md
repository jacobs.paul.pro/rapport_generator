# TP4  ALLOCATIONS AUTOMATIQUES ET SEGMENT DE PILE

(Jacobs Paul, Pype Gwennhaelle, Audabram Luc)
## 1 Fonction Main

command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -Wall -m32 main.c
```
command : 
```bash
cat main.s
```
<details>
<summary> result of cat main.s</summary>

```assembly
	.file	"main.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr32
	pushl	%ebp
	movl	%esp, %ebp
	call	__x86.get_pc_thunk.ax
	addl	$_GLOBAL_OFFSET_TABLE_, %eax
	movl	$0, %eax
	popl	%ebp
	ret
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
	.globl	__x86.get_pc_thunk.ax
	.hidden	__x86.get_pc_thunk.ax
	.type	__x86.get_pc_thunk.ax, @function
__x86.get_pc_thunk.ax:
	movl	(%esp), %eax
	ret
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 4
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 4
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 4
4:
```
</details>


On observe un PUSH et un POP. Le push permet de lire la valeur dans le registre indiqué ici ebp(BP) et de mettre cette valeur sur le dessus de la pile Le pop permet de récupérer la derniere valeur de la pile et de la déplacer dans le registre spécifié ici ebp(BP) ce qui induit une suppression de cette induit la suppression de cette valeur  
* Compléter (au crayon) le schéma de la pile sur la page suivante en précisant quel est son contenu suite à l'exécution du programme jusqu'à l'instruction ret en fin du main. Ne pas oublier qu'avant le début de notre programme, le code de la fonction de startup s'est exécuté.

* Proposer une réécriture des instructions CISC-like (Complex Instruction Set Computing) push et pop à l'aide des instructions RISC-like (Reduce Instruction Set Computing) sub, add et mov

Pour la fonction PUSH
    sub $4, %esp
    movl %ebp, (%esp)
Pour la fonction POP
    movl (%esp), %ebp
    add $4, %esp


## 2 Variables locales initialisés

command : 
```bash
cat local_variable_init.c
```
<details>
<summary> result of cat local_variable_init.c</summary>

```assembly
int main(void)
{
	char a=1;
	short int b=2;
	int c=3;
	long int d=4;
	
	//c = (int) a;
	
	return 0;
}



```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -Wall -m32 local_variable_init.c
```
command : 
```bash
cat local_variable_init.s
```
<details>
<summary> result of cat local_variable_init.s</summary>

```assembly
	.file	"local_variable_init.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr32
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	call	__x86.get_pc_thunk.ax
	addl	$_GLOBAL_OFFSET_TABLE_, %eax
	movb	$1, -11(%ebp)
	movw	$2, -10(%ebp)
	movl	$3, -8(%ebp)
	movl	$4, -4(%ebp)
	movl	$0, %eax
	leave
	ret
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
	.globl	__x86.get_pc_thunk.ax
	.hidden	__x86.get_pc_thunk.ax
	.type	__x86.get_pc_thunk.ax, @function
__x86.get_pc_thunk.ax:
	movl	(%esp), %eax
	ret
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 4
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 4
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 4
4:
```
</details>


* Compléter (au crayon) le schéma de la pile sur la page précédente en précisant quel est son contenu suite à l'exécution du programme jusqu'à l'instruction ret en fin du main.

* Préciser les tailles ou empreintes mémoire des variables locales a,b,c et d

La taille de chaque variable locale est :

"a": 1 octet,

"b": 2 octets,

"c" et "d" : 4 octets 

* Préciser les adresses relatives des variables locales a,b,c et d


"a" est situé à l'adresse -11(%epb),

"b" est situé à l'adresse -10(%ebp),

"c" est situé à l'adresse -8(%epb),

"d" est situé à l'adresse -4(%epb)

* Combien faut-il de pointeur, et donc de registre pour le stocker, afin de gérer et d'adresser une infinité de variables locales ?

Nous n'avons besoin que d'un pointeur sur lequel est réalisé un décalage et d'un registre
* Dé-commenter le cast (transtypage) présent dans le programme, compiler et analyser le fichier assembleur de sortie. Analyser le résultat

command : 
```bash
cat local_variable_init_cast.c
```
<details>
<summary> result of cat local_variable_init_cast.c</summary>

```assembly
int main(void)
{
	char a=1;
	short int b=2;
	int c=3;
	long int d=4;
	
	c = (int) a;
	
	return 0;
}



```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -Wall -m32 local_variable_init_cast.c
```
command : 
```bash
cat local_variable_init_cast.s
```
<details>
<summary> result of cat local_variable_init_cast.s</summary>

```assembly
	.file	"local_variable_init_cast.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr32
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	call	__x86.get_pc_thunk.ax
	addl	$_GLOBAL_OFFSET_TABLE_, %eax
	movb	$1, -11(%ebp)
	movw	$2, -10(%ebp)
	movl	$3, -8(%ebp)
	movl	$4, -4(%ebp)
	movsbl	-11(%ebp), %eax
	movl	%eax, -8(%ebp)
	movl	$0, %eax
	leave
	ret
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
	.globl	__x86.get_pc_thunk.ax
	.hidden	__x86.get_pc_thunk.ax
	.type	__x86.get_pc_thunk.ax, @function
__x86.get_pc_thunk.ax:
	movl	(%esp), %eax
	ret
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 4
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 4
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 4
4:
```
</details>


On remarque l'apparition de deux lignes 
    movsbl	-11(%ebp), %eax --> qui correspond au cast en int de la valeur de la variable a et dont le resultat est stocké dans le registre EAX
	movl	%eax, -8(%ebp) --> la valeur stocké dans le registre EAX est ensuite mise dans la variable c qui est situé dans -11(%epb)
* Qu'est-ce qu'une extension de signe en arithmétique entière signée Cà2 (Complément à 2)

C'est un processus qui permet de convertir le signé dans un format qui sera plus large, il agit donc en tant que casts
* Qualifier le type de la variable a de const. Compiler le programme puis interpréter le résultat. Que constatons-nous ?

command : 
```bash
cat local_variable_init_const.c
```
<details>
<summary> result of cat local_variable_init_const.c</summary>

```assembly
int main(void)
{
	const char a=1;
	short int b=2;
	int c=3;
	long int d=4;
	
	//c = (int) a;
	
	return 0;
}



```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -Wall -m32 local_variable_init_const.c
```
command : 
```bash
cat local_variable_init_const.s
```
<details>
<summary> result of cat local_variable_init_const.s</summary>

```assembly
	.file	"local_variable_init_const.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr32
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	call	__x86.get_pc_thunk.ax
	addl	$_GLOBAL_OFFSET_TABLE_, %eax
	movb	$1, -11(%ebp)
	movw	$2, -10(%ebp)
	movl	$3, -8(%ebp)
	movl	$4, -4(%ebp)
	movl	$0, %eax
	leave
	ret
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
	.globl	__x86.get_pc_thunk.ax
	.hidden	__x86.get_pc_thunk.ax
	.type	__x86.get_pc_thunk.ax, @function
__x86.get_pc_thunk.ax:
	movl	(%esp), %eax
	ret
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 4
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 4
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 4
4:
```
</details>


* Quel est le rôle du qualificateur de type const et donc son usage ?

La qualificateur de type constant permet de faire en sorte que la valeur de notre variable ne change pas. 
C'est utilisé quand on a des paramètres qui ne change pas dans le programme.


## 3 Variables locales non-initialisés

* Ouvrir puis compiler le fichier local_variable_uninit.c en s'arrêtant à la phase d'assemblage. Analyser le fichier assembleur généré.

command : 
```bash
cat local_variable_uninit.c
```
<details>
<summary> result of cat local_variable_uninit.c</summary>

```assembly
int main(void)
{
	char a;
	short int b;
	int c;
	long int d;
	
	return 0;
}

```
</details>


command : 
```bash
 gcc -S -fno-asynchronous-unwind-tables -Wall -m32 local_variable_uninit.c
```
command : 
```bash
cat local_variable_uninit.s
```
<details>
<summary> result of cat local_variable_uninit.s</summary>

```assembly
	.file	"local_variable_uninit.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr32
	pushl	%ebp
	movl	%esp, %ebp
	call	__x86.get_pc_thunk.ax
	addl	$_GLOBAL_OFFSET_TABLE_, %eax
	movl	$0, %eax
	popl	%ebp
	ret
	.size	main, .-main
	.section	.text.__x86.get_pc_thunk.ax,"axG",@progbits,__x86.get_pc_thunk.ax,comdat
	.globl	__x86.get_pc_thunk.ax
	.hidden	__x86.get_pc_thunk.ax
	.type	__x86.get_pc_thunk.ax, @function
__x86.get_pc_thunk.ax:
	movl	(%esp), %eax
	ret
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 4
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 4
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 4
4:
```
</details>


*  Que constatons-nous ?

Les variables n'ayant pas été initialisées, elles n'apparaisent pas dans le language assembleur car nous n'avons pas la nécessité de les ajouter à la pile. 
Le déplacement de ESP n'est pas nécassaire et n'apparait donc pas non plus.
De plus un pop est effectué à la fin
* Ajouter le qualificateur de type volatile devant chaque déclaration, compiler le programme puis interpréter le résultat.

* Quel est le rôle de ce qualificateur de type volatile et donc son usage ? S'aider d'internet

Un qualificateur définit le type de la variable. Et volatile signifi que ce n'est pas statique c'est à dire que ça disparait à la fermeture de la fonction dans laquelle la variable a été déclarée et que les valeurs peuvent être modifié


## 4 Debugger

command : 
```bash
gcc -g -fno-asynchronous-unwind-tables -Wall -m32 local_variable_init.c -o local_variable_init
```
command : 
```bash
gdb ./local_variable_init
```
## 5 Appel et parametre de fonction

command : 
```bash
cat function_parameters.c
```
<details>
<summary> result of cat function_parameters.c</summary>

```assembly
/* ANSI C standard syntax - 1989 */
void function_1 (void) ;
int function_2 (int a, int b, int c);

int main(void)
{

	function_1();

return 0;
}

void function_1 (void) 
{
	int ret_1;
	
	ret_1 = function_2 (1, 2, 3);
}

/* K&R C original syntax - 1978 */
int function_2 (a_2, b_2, c_2) 
int a_2;
int b_2;
int c_2;
{
	return a_2 + b_2 + c_2;
}
```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -Wall function_parameters.c
```
command : 
```bash
cat function_parameters.s
```
<details>
<summary> result of cat function_parameters.s</summary>

```assembly
	.file	"function_parameters.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	call	function_1
	movl	$0, %eax
	popq	%rbp
	ret
	.size	main, .-main
	.globl	function_1
	.type	function_1, @function
function_1:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movl	$3, %edx
	movl	$2, %esi
	movl	$1, %edi
	call	function_2
	movl	%eax, -4(%rbp)
	nop
	leave
	ret
	.size	function_1, .-function_1
	.globl	function_2
	.type	function_2, @function
function_2:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	%edx, -12(%rbp)
	movl	-4(%rbp), %edx
	movl	-8(%rbp), %eax
	addl	%eax, %edx
	movl	-12(%rbp), %eax
	addl	%edx, %eax
	popq	%rbp
	ret
	.size	function_2, .-function_2
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
```
</details>


* Compléter (au crayon) le schéma de la pile sur la page suivante en précisant quel est son contenu exhaustif suite à l'exécution du programme jusqu'à l'instruction ret en fin du main.

* Pour le passage d'arguments de type entier, quels sont respectivement les 3 registres CPU utilisés par GCC pour passer les paramètres à une fonction appelée? Quel est par défaut le registre utilisé pour passer une valeur de retour entière ?

Les 3 registres utilisé sont EAX, SP et BP. Le registre utilisé pour passer une valeur de retour entière est le registre EAX.
* Quelles sont les adresses relatives des variables ret_1 (variable locale à function_1) et a_2(variable locale à function_2) ? Pourquoi ne sont-elles pas spatialement au même emplacement mémoire sur la pile ?

Les adresses des variable de la function_1 sont stocké dans ret_1 situé à l'adresse -4(%rpb) de la pile 
La variable a_2 de la function_2 est situé dans à l'adresse -4(%rbop) 
Ces variables appartiennent à deux fonctions différentes elles ne peuvent donc pas être stockées au même endroit car le BP et le SP ont bougé entre les deux fonctions
* Observer la définition de la fonction function_2 utilisant la syntaxe K&R (Kernighan & Ritchie) originelle du langage C . Au final, qu'est-ce qu'un paramètre de fonction

Un paramètre de fonction ce sont des arguments que l'on fournit à la fonction comme a_2, b_2 et c_2

## 6 Fonction inline et optimisation

command : 
```bash
cat function_inlining.c
```
<details>
<summary> result of cat function_inlining.c</summary>

```assembly

void swap(int* pt_a, int* pt_b);

/*void swap(register int* pt_a, register int* pt_b);*/

/*inline void swap(int* pt_a, int* pt_b) __attribute__((always_inline));*/


int main(void)
{
	int a=1, b=2;

	swap(&a, &b);

return 0;
}

void swap(int* pt_a, int* pt_b)
/*void swap(register int* pt_a,register  int* pt_b)*/
/*inline void swap(int*  pt_a, int* pt_b)*/

{
	int tmp;
	
	tmp = *pt_a;
	*pt_a = *pt_b;
	*pt_b = tmp;
}

```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -fno-stack-protector -Wall function_inlining.c
```
command : 
```bash
cat function_inlining.s
```
<details>
<summary> result of cat function_inlining.s</summary>

```assembly
	.file	"function_inlining.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movl	$1, -4(%rbp)
	movl	$2, -8(%rbp)
	leaq	-8(%rbp), %rdx
	leaq	-4(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	swap
	movl	$0, %eax
	leave
	ret
	.size	main, .-main
	.globl	swap
	.type	swap, @function
swap:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -4(%rbp)
	movq	-32(%rbp), %rax
	movl	(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-32(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
	nop
	popq	%rbp
	ret
	.size	swap, .-swap
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
```
</details>


* Compléter (au crayon) le schéma de la pile sur la page suivante en précisant quel est son contenu exhaustif suite à l'exécution du programme jusqu'à l'instruction ret en fin du main. 

* Dé-commenter le prototype de la fonction swap utilisant la classe de stockage register pour la déclaration des paramètres de fonction et commenter le prototype générique. Faire de même au niveau de la définition de fonction. Qualifier également la variable tmp dans la fonction swap de register (à laisser jusqu'à la fin de l'exercice). compiler et analyser le code assembleur de la fonction swap. Quel est le rôle et l'intérêt de cette classe de stockage ?

//void swap(int* pt_a, int* pt_b);
void swap(register int* pt_a, register int* pt_b);
//inline void swap(int* pt_a, int* pt_b) __attribute__((always_inline));
command : 
```bash
cat function_inlining_swap.c
```
<details>
<summary> result of cat function_inlining_swap.c</summary>

```assembly
void swap(int* pt_a, int* pt_b);
/*void swap(register int* pt_a, register int* pt_b);*/
/*inline void swap(int* pt_a, int* pt_b) __attribute__((always_inline));*/
int main(void)
{
	int a=1, b=2;

	swap(&a, &b);

return 0;
}


/*void swap(int* pt_a, int* pt_b)*/
void swap(register int* pt_a,register  int* pt_b)
/*inline void swap(int*  pt_a, int* pt_b)*/
{
	int tmp;
	
	tmp = *pt_a;
	*pt_a = *pt_b;
	*pt_b = tmp;
}

```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -fno-stack-protector -Wall function_inlining_swap.c
```
command : 
```bash
cat function_inlining_swap.s
```
<details>
<summary> result of cat function_inlining_swap.s</summary>

```assembly
	.file	"function_inlining_swap.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movl	$1, -4(%rbp)
	movl	$2, -8(%rbp)
	leaq	-8(%rbp), %rdx
	leaq	-4(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	swap
	movl	$0, %eax
	leave
	ret
	.size	main, .-main
	.globl	swap
	.type	swap, @function
swap:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	movq	%rdi, %rdx
	movq	%rsi, %rax
	movl	(%rdx), %ecx
	movl	%ecx, -4(%rbp)
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
	nop
	popq	%rbp
	ret
	.size	swap, .-swap
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
```
</details>


* Lever les options d'optimisation de niveau 1  (-O1) de GGC et analyser la sortie. Quel usage est fait de la pile pour nos variables locales

Les variables locales sont placées en variables globale dans le main.
command : 
```bash
gcc -S -O1 -fno-asynchronous-unwind-tables -fno-stack-protector -Wall function_inlining.c
```
* Retirer les options d'optimisation de GGC, dé-commenter le prototype de la fonction swapqualifiée de inline (seulement à la déclaration)et commenter les prototypes génériques. Analyser le code assembleur de la fonction main. Quel est le rôle du mot clé inline arrivé avec lanorme C99 en 1999 ? Pourquoi le code de la fonction swap est-il toujours présent dans le programme assembleur et donc à terme dans le firmware alors que la fonction n'est plus appelée depuis le main?

//void swap(int* pt_a, int* pt_b);

//void swap(register int* pt_a, register int* pt_b);

inline void swap(int* pt_a, int* pt_b) __attribute__((always_inline));

command : 
```bash
cat function_inlining_inline.c
```
<details>
<summary> result of cat function_inlining_inline.c</summary>

```assembly
void swap(int* pt_a, int* pt_b);
/*void swap(register int* pt_a, register int* pt_b);*/
inline void swap(int* pt_a, int* pt_b) __attribute__((always_inline));

int main(void)
{
	int a=1, b=2;

	swap(&a, &b);

return 0;
}

/*void swap(int* pt_a, int* pt_b)*/

void swap(register int* pt_a,register  int* pt_b)

/*inline void swap(int*  pt_a, int* pt_b)*/

{
	int tmp;
	
	tmp = *pt_a;
	*pt_a = *pt_b;
	*pt_b = tmp;
}

```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -fno-stack-protector -Wall function_inlining_inline.c
```
command : 
```bash
cat function_inlining_inline.s
```
<details>
<summary> result of cat function_inlining_inline.s</summary>

```assembly
	.file	"function_inlining_inline.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	movl	$1, -24(%rbp)
	movl	$2, -28(%rbp)
	leaq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	leaq	-28(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -20(%rbp)
	movq	-16(%rbp), %rax
	movl	(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, (%rax)
	movq	-16(%rbp), %rax
	movl	-20(%rbp), %edx
	movl	%edx, (%rax)
	nop
	movl	$0, %eax
	popq	%rbp
	ret
	.size	main, .-main
	.globl	swap
	.type	swap, @function
swap:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	movq	%rdi, %rdx
	movq	%rsi, %rax
	movl	(%rdx), %ecx
	movl	%ecx, -4(%rbp)
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
	nop
	popq	%rbp
	ret
	.size	swap, .-swap
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
```
</details>


Le mot clef inline est un attribut qui sert aux fonctions qui ont pour but de remplacer le système macro qui est défini grâce à #define (commande du processeur)
La fonction swap est écrite dans le main parce ce qu'elle est déclarée dans le uncline. Cependant elle n'est pas construite dedans donc elle est créée hors du main dans l'assembleur.
* Dé-commenter le prototype de la fonction swap qualifiée de inline au niveau de la définition dela fonction swap et commenter les prototypes génériques. Analyser le code assembleur généré.Verdict 

//void swap(int* pt_a, int* pt_b)

//void swap(register int* pt_a, register int* pt_b)

inline void swap(int* pt_a, int* pt_b){...}

command : 
```bash
cat function_inlining_inline_function.c
```
<details>
<summary> result of cat function_inlining_inline_function.c</summary>

```assembly
void swap(int* pt_a, int* pt_b);
/*void swap(register int* pt_a, register int* pt_b);*/
inline void swap(int* pt_a, int* pt_b) __attribute__((always_inline));

int main(void)
{
	int a=1, b=2;

	swap(&a, &b);

return 0;
}

/*void swap(int* pt_a, int* pt_b)*/
/*void swap(register int* pt_a,register  int* pt_b)*/
inline void swap(int*  pt_a, int* pt_b)
{
	int tmp;
	
	tmp = *pt_a;
	*pt_a = *pt_b;
	*pt_b = tmp;
}

```
</details>


command : 
```bash
gcc -S -fno-asynchronous-unwind-tables -fno-stack-protector -Wall function_inlining_inline_function.c
```
command : 
```bash
cat function_inlining_inline_function.s
```
<details>
<summary> result of cat function_inlining_inline_function.s</summary>

```assembly
	.file	"function_inlining_inline_function.c"
	.text
	.globl	main
	.type	main, @function
main:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	movl	$1, -24(%rbp)
	movl	$2, -28(%rbp)
	leaq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	leaq	-28(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -20(%rbp)
	movq	-16(%rbp), %rax
	movl	(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, (%rax)
	movq	-16(%rbp), %rax
	movl	-20(%rbp), %edx
	movl	%edx, (%rax)
	nop
	movl	$0, %eax
	popq	%rbp
	ret
	.size	main, .-main
	.globl	swap
	.type	swap, @function
swap:
	endbr64
	pushq	%rbp
	movq	%rsp, %rbp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -4(%rbp)
	movq	-32(%rbp), %rax
	movl	(%rax), %edx
	movq	-24(%rbp), %rax
	movl	%edx, (%rax)
	movq	-32(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
	nop
	popq	%rbp
	ret
	.size	swap, .-swap
	.ident	"GCC: (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
```
</details>


## 7 Limite de la pile 

* Nous allons maintenant tester les limites de notre pile applicative. Se déplacer dans le répertoire disco/except/. Compiler le fichier stackoverflow.c puis l'exécuter

command : 
```bash
gcc stackoverflow.c -o stackoverflow
```
* Quel segment logique mémoire virtuel applicatif a subit un débordement et a causé la fin de notre programme? Question facile!

Le segment qui a subit mun débordement est le segment 8379673
* Qu'elle est la taille par défaut de la pile associée à notre programme ? Quelle entité fixe et  impose la taille de la pile associée à un programme?

La taille de la pile associé est de 48M
* Dé-commenter la section de code présente dans la fonction goto_segmentation_fault. Compiler à nouveau le fichier stackoverflow.c puis l'exécuter. Qu'elle est la nouvelle taille de la pile associée à notre programme

La taille une fois la fonction décommenté est de 16M
