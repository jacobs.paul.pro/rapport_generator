# 6 ALLOCATIONS DYNAMIQUES ET SEGMENT DE TAS

## 1  Gestion du tas

command : 
```bash
gcc -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall -static heap.c -o heap
```
command : 
```bash
./heap
```
command : 
```bash
[CTRL] + [c]
```
command : 
```bash
ps -a
```
<details>
<summary> result of ps -a</summary>

```assembly
    PID TTY          TIME CMD
   8167 pts/1    00:00:00 python3
   8173 pts/1    00:00:00 ps
```
</details>


command : 
```bash
cat /proc/<pid_of_heap_program>/maps
```
* Qu’elle est la taille du segment de pile ?

* Qu’elle est la taille du segment de tas ?

* Qu’elle est la taille du segment de code (seul segment statique dont les propriétés permettent l’exécution, propriétés --x-) ? Constater que chaque segment en mémoire principale possède une taille multiple de 4Ko, la taille d’une page gérée par l’unité de pagination MMU.

* Compiler le fichier heap.c en s’arrêtant à l’assemblage. Analyser le programme assembleur généré. A ce stade de l’enseignement, vous devez pouvoir être capable d’analyser des script assembleur de ce type. Si c’est le cas, ce que j’espère, bravo, du chemin a été fait !

command : 
```bash
 gcc -S -fno-asynchronous-unwind-tables -fno-pie -fno-stack-protector -fcf-protection=none -Wall heap.c
```
## 2  Limites du tas

* Se placer dans le répertoire disco/except. Compiler le fichier heap_overflow.c jusqu’à l’édition des liens incluse et l’exécuter. Analyser le programme et observer les limites du tas. Comparer aux informations proposées par le système.

command : 
```bash
gcc heap_overflow.c -o heap_overflow
```
command : 
```bash
./heap_overflow
```
command : 
```bash
free
```
command : 
```bash
cat /proc/meminfo
```
