## 2  Limites du tas

* Se placer dans le répertoire disco/except. Compiler le fichier heap_overflow.c jusqu’à l’édition des liens incluse et l’exécuter. Analyser le programme et observer les limites du tas. Comparer aux informations proposées par le système.

command : 
```bash
gcc heap_overflow.c -o heap_overflow
```
command : 
```bash
objdump -S heap_overflow
```
<details>
<summary> result of objdump -S heap_overflow</summary>

```assembly

heap_overflow:     file format elf64-x86-64


Disassembly of section .init:

0000000000001000 <_init>:
    1000:	f3 0f 1e fa          	endbr64 
    1004:	48 83 ec 08          	sub    $0x8,%rsp
    1008:	48 8b 05 d9 2f 00 00 	mov    0x2fd9(%rip),%rax        # 3fe8 <__gmon_start__>
    100f:	48 85 c0             	test   %rax,%rax
    1012:	74 02                	je     1016 <_init+0x16>
    1014:	ff d0                	call   *%rax
    1016:	48 83 c4 08          	add    $0x8,%rsp
    101a:	c3                   	ret    

Disassembly of section .plt:

0000000000001020 <free@plt-0x10>:
    1020:	ff 35 e2 2f 00 00    	push   0x2fe2(%rip)        # 4008 <_GLOBAL_OFFSET_TABLE_+0x8>
    1026:	ff 25 e4 2f 00 00    	jmp    *0x2fe4(%rip)        # 4010 <_GLOBAL_OFFSET_TABLE_+0x10>
    102c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000001030 <free@plt>:
    1030:	ff 25 e2 2f 00 00    	jmp    *0x2fe2(%rip)        # 4018 <free@GLIBC_2.2.5>
    1036:	68 00 00 00 00       	push   $0x0
    103b:	e9 e0 ff ff ff       	jmp    1020 <_init+0x20>

0000000000001040 <printf@plt>:
    1040:	ff 25 da 2f 00 00    	jmp    *0x2fda(%rip)        # 4020 <printf@GLIBC_2.2.5>
    1046:	68 01 00 00 00       	push   $0x1
    104b:	e9 d0 ff ff ff       	jmp    1020 <_init+0x20>

0000000000001050 <malloc@plt>:
    1050:	ff 25 d2 2f 00 00    	jmp    *0x2fd2(%rip)        # 4028 <malloc@GLIBC_2.2.5>
    1056:	68 02 00 00 00       	push   $0x2
    105b:	e9 c0 ff ff ff       	jmp    1020 <_init+0x20>

Disassembly of section .text:

0000000000001060 <_start>:
    1060:	f3 0f 1e fa          	endbr64 
    1064:	31 ed                	xor    %ebp,%ebp
    1066:	49 89 d1             	mov    %rdx,%r9
    1069:	5e                   	pop    %rsi
    106a:	48 89 e2             	mov    %rsp,%rdx
    106d:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
    1071:	50                   	push   %rax
    1072:	54                   	push   %rsp
    1073:	4c 8d 05 c6 01 00 00 	lea    0x1c6(%rip),%r8        # 1240 <__libc_csu_fini>
    107a:	48 8d 0d 4f 01 00 00 	lea    0x14f(%rip),%rcx        # 11d0 <__libc_csu_init>
    1081:	48 8d 3d d1 00 00 00 	lea    0xd1(%rip),%rdi        # 1159 <main>
    1088:	ff 15 52 2f 00 00    	call   *0x2f52(%rip)        # 3fe0 <__libc_start_main@GLIBC_2.2.5>
    108e:	f4                   	hlt    
    108f:	90                   	nop

0000000000001090 <deregister_tm_clones>:
    1090:	48 8d 3d a9 2f 00 00 	lea    0x2fa9(%rip),%rdi        # 4040 <__TMC_END__>
    1097:	48 8d 05 a2 2f 00 00 	lea    0x2fa2(%rip),%rax        # 4040 <__TMC_END__>
    109e:	48 39 f8             	cmp    %rdi,%rax
    10a1:	74 15                	je     10b8 <deregister_tm_clones+0x28>
    10a3:	48 8b 05 2e 2f 00 00 	mov    0x2f2e(%rip),%rax        # 3fd8 <_ITM_deregisterTMCloneTable>
    10aa:	48 85 c0             	test   %rax,%rax
    10ad:	74 09                	je     10b8 <deregister_tm_clones+0x28>
    10af:	ff e0                	jmp    *%rax
    10b1:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
    10b8:	c3                   	ret    
    10b9:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000000010c0 <register_tm_clones>:
    10c0:	48 8d 3d 79 2f 00 00 	lea    0x2f79(%rip),%rdi        # 4040 <__TMC_END__>
    10c7:	48 8d 35 72 2f 00 00 	lea    0x2f72(%rip),%rsi        # 4040 <__TMC_END__>
    10ce:	48 29 fe             	sub    %rdi,%rsi
    10d1:	48 89 f0             	mov    %rsi,%rax
    10d4:	48 c1 ee 3f          	shr    $0x3f,%rsi
    10d8:	48 c1 f8 03          	sar    $0x3,%rax
    10dc:	48 01 c6             	add    %rax,%rsi
    10df:	48 d1 fe             	sar    %rsi
    10e2:	74 14                	je     10f8 <register_tm_clones+0x38>
    10e4:	48 8b 05 05 2f 00 00 	mov    0x2f05(%rip),%rax        # 3ff0 <_ITM_registerTMCloneTable>
    10eb:	48 85 c0             	test   %rax,%rax
    10ee:	74 08                	je     10f8 <register_tm_clones+0x38>
    10f0:	ff e0                	jmp    *%rax
    10f2:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)
    10f8:	c3                   	ret    
    10f9:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

0000000000001100 <__do_global_dtors_aux>:
    1100:	f3 0f 1e fa          	endbr64 
    1104:	80 3d 35 2f 00 00 00 	cmpb   $0x0,0x2f35(%rip)        # 4040 <__TMC_END__>
    110b:	75 33                	jne    1140 <__do_global_dtors_aux+0x40>
    110d:	55                   	push   %rbp
    110e:	48 83 3d e2 2e 00 00 	cmpq   $0x0,0x2ee2(%rip)        # 3ff8 <__cxa_finalize@GLIBC_2.2.5>
    1115:	00 
    1116:	48 89 e5             	mov    %rsp,%rbp
    1119:	74 0d                	je     1128 <__do_global_dtors_aux+0x28>
    111b:	48 8b 3d 16 2f 00 00 	mov    0x2f16(%rip),%rdi        # 4038 <__dso_handle>
    1122:	ff 15 d0 2e 00 00    	call   *0x2ed0(%rip)        # 3ff8 <__cxa_finalize@GLIBC_2.2.5>
    1128:	e8 63 ff ff ff       	call   1090 <deregister_tm_clones>
    112d:	c6 05 0c 2f 00 00 01 	movb   $0x1,0x2f0c(%rip)        # 4040 <__TMC_END__>
    1134:	5d                   	pop    %rbp
    1135:	c3                   	ret    
    1136:	66 2e 0f 1f 84 00 00 	cs nopw 0x0(%rax,%rax,1)
    113d:	00 00 00 
    1140:	c3                   	ret    
    1141:	66 66 2e 0f 1f 84 00 	data16 cs nopw 0x0(%rax,%rax,1)
    1148:	00 00 00 00 
    114c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000001150 <frame_dummy>:
    1150:	f3 0f 1e fa          	endbr64 
    1154:	e9 67 ff ff ff       	jmp    10c0 <register_tm_clones>

0000000000001159 <main>:
    1159:	55                   	push   %rbp
    115a:	48 89 e5             	mov    %rsp,%rbp
    115d:	48 83 ec 20          	sub    $0x20,%rsp
    1161:	89 7d ec             	mov    %edi,-0x14(%rbp)
    1164:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
    1168:	48 c7 45 f0 00 00 00 	movq   $0x0,-0x10(%rbp)
    116f:	00 
    1170:	48 c7 45 f8 00 00 00 	movq   $0x0,-0x8(%rbp)
    1177:	00 
    1178:	48 81 45 f0 00 00 10 	addq   $0x100000,-0x10(%rbp)
    117f:	00 
    1180:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1184:	48 89 c7             	mov    %rax,%rdi
    1187:	e8 c4 fe ff ff       	call   1050 <malloc@plt>
    118c:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1190:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1194:	c6 00 00             	movb   $0x0,(%rax)
    1197:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    119b:	48 89 c6             	mov    %rax,%rsi
    119e:	48 8d 3d 5f 0e 00 00 	lea    0xe5f(%rip),%rdi        # 2004 <_IO_stdin_used+0x4>
    11a5:	b8 00 00 00 00       	mov    $0x0,%eax
    11aa:	e8 91 fe ff ff       	call   1040 <printf@plt>
    11af:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    11b4:	74 0c                	je     11c2 <main+0x69>
    11b6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    11ba:	48 89 c7             	mov    %rax,%rdi
    11bd:	e8 6e fe ff ff       	call   1030 <free@plt>
    11c2:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    11c7:	75 af                	jne    1178 <main+0x1f>
    11c9:	b8 00 00 00 00       	mov    $0x0,%eax
    11ce:	c9                   	leave  
    11cf:	c3                   	ret    

00000000000011d0 <__libc_csu_init>:
    11d0:	f3 0f 1e fa          	endbr64 
    11d4:	41 57                	push   %r15
    11d6:	4c 8d 3d 0b 2c 00 00 	lea    0x2c0b(%rip),%r15        # 3de8 <__frame_dummy_init_array_entry>
    11dd:	41 56                	push   %r14
    11df:	49 89 d6             	mov    %rdx,%r14
    11e2:	41 55                	push   %r13
    11e4:	49 89 f5             	mov    %rsi,%r13
    11e7:	41 54                	push   %r12
    11e9:	41 89 fc             	mov    %edi,%r12d
    11ec:	55                   	push   %rbp
    11ed:	48 8d 2d fc 2b 00 00 	lea    0x2bfc(%rip),%rbp        # 3df0 <__do_global_dtors_aux_fini_array_entry>
    11f4:	53                   	push   %rbx
    11f5:	4c 29 fd             	sub    %r15,%rbp
    11f8:	48 83 ec 08          	sub    $0x8,%rsp
    11fc:	e8 ff fd ff ff       	call   1000 <_init>
    1201:	48 c1 fd 03          	sar    $0x3,%rbp
    1205:	74 1f                	je     1226 <__libc_csu_init+0x56>
    1207:	31 db                	xor    %ebx,%ebx
    1209:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
    1210:	4c 89 f2             	mov    %r14,%rdx
    1213:	4c 89 ee             	mov    %r13,%rsi
    1216:	44 89 e7             	mov    %r12d,%edi
    1219:	41 ff 14 df          	call   *(%r15,%rbx,8)
    121d:	48 83 c3 01          	add    $0x1,%rbx
    1221:	48 39 dd             	cmp    %rbx,%rbp
    1224:	75 ea                	jne    1210 <__libc_csu_init+0x40>
    1226:	48 83 c4 08          	add    $0x8,%rsp
    122a:	5b                   	pop    %rbx
    122b:	5d                   	pop    %rbp
    122c:	41 5c                	pop    %r12
    122e:	41 5d                	pop    %r13
    1230:	41 5e                	pop    %r14
    1232:	41 5f                	pop    %r15
    1234:	c3                   	ret    
    1235:	66 66 2e 0f 1f 84 00 	data16 cs nopw 0x0(%rax,%rax,1)
    123c:	00 00 00 00 

0000000000001240 <__libc_csu_fini>:
    1240:	f3 0f 1e fa          	endbr64 
    1244:	c3                   	ret    

Disassembly of section .fini:

0000000000001248 <_fini>:
    1248:	f3 0f 1e fa          	endbr64 
    124c:	48 83 ec 08          	sub    $0x8,%rsp
    1250:	48 83 c4 08          	add    $0x8,%rsp
    1254:	c3                   	ret    
```
</details>


command : 
```bash
./heap_overflow
```
command : 
```bash
free -h
```
<details>
<summary> result of free -h</summary>

```assembly
               total        used        free      shared  buff/cache   available
Mem:            30Gi       2.7Gi        24Gi       649Mi       3.3Gi        27Gi
Swap:             0B          0B          0B
```
</details>


command : 
```bash
cat /proc/meminfo
```
<details>
<summary> result of cat /proc/meminfo</summary>

```assembly
MemTotal:       32479068 kB
MemFree:        26124656 kB
MemAvailable:   28530352 kB
Buffers:          180332 kB
Cached:          3189784 kB
SwapCached:            0 kB
Active:          1385780 kB
Inactive:        4109384 kB
Active(anon):      15596 kB
Inactive(anon):  2774944 kB
Active(file):    1370184 kB
Inactive(file):  1334440 kB
Unevictable:      384740 kB
Mlocked:              48 kB
SwapTotal:             0 kB
SwapFree:              0 kB
Dirty:               588 kB
Writeback:            16 kB
AnonPages:       2510480 kB
Mapped:           887456 kB
Shmem:            665464 kB
KReclaimable:     139392 kB
Slab:             229848 kB
SReclaimable:     139392 kB
SUnreclaim:        90456 kB
KernelStack:       14864 kB
PageTables:        37404 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    16239532 kB
Committed_AS:   10741964 kB
VmallocTotal:   34359738367 kB
VmallocUsed:       92512 kB
VmallocChunk:          0 kB
Percpu:             8896 kB
HardwareCorrupted:     0 kB
AnonHugePages:         0 kB
ShmemHugePages:        0 kB
ShmemPmdMapped:        0 kB
FileHugePages:         0 kB
FilePmdMapped:         0 kB
CmaTotal:              0 kB
CmaFree:               0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
Hugetlb:               0 kB
DirectMap4k:      410184 kB
DirectMap2M:    10717184 kB
DirectMap1G:    22020096 kB
```
</details>


